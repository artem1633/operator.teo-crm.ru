<?php

use yii\db\Migration;

/**
 * Class m181005_110659_add_columns_to_drivers_table
 */
class m181005_110659_add_columns_to_drivers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('drivers', 'percenttocharge', $this->float()->comment('Комиссия водителя'));
        $this->addColumn('drivers', 'driver_status', $this->string()->comment('Статус водителя. A - допущен, B - заблокирован'));
        $this->addColumn('drivers', 'creditlimit', $this->integer()->comment('Лимит баланса водителя'));
        $this->addColumn('drivers', 'is_test', $this->integer()->comment('Тестовый ли водитель'));
        $this->addColumn('drivers', 'invited_by', $this->integer()->comment('ID пригласившего водителя в компании'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('drivers', 'percenttocharge');
        $this->dropColumn('drivers', 'driver_status');
        $this->dropColumn('drivers', 'creditlimit');
        $this->dropColumn('drivers', 'is_test');
        $this->dropColumn('drivers', 'invited_by');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181005_110659_add_columns_to_drivers_table cannot be reverted.\n";

        return false;
    }
    */
}
