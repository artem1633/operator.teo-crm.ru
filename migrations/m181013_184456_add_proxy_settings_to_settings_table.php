<?php

use yii\db\Migration;

/**
 * Class m181013_184456_add_proxy_settings_to_settings_table
 */
class m181013_184456_add_proxy_settings_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'name' => 'IP адрес прокси сервера',
            'key' => 'ip_proxy',
        ]);
        $this->insert('settings', [
            'name' => 'Порт прокси сервера',
            'key' => 'port_proxy',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181013_184456_add_proxy_settings_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181013_184456_add_proxy_settings_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
