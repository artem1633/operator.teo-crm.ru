<?php

use yii\db\Migration;

/**
 * Class m181011_115019_change_foreignkey_to_drivers_table
 */
class m181011_115019_change_foreignkey_to_drivers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-drivers-creator_id', 'drivers');
        $this->addForeignKey("fk-drivers-creator_id", "drivers", "creator_id", "users", "id", 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181011_115019_change_foreignkey_to_drivers_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181011_115019_change_foreignkey_to_drivers_table cannot be reverted.\n";

        return false;
    }
    */
}
