<?php

use yii\db\Migration;

/**
 * Class m181015_115801_add_status_report_to_settings_table
 */
class m181015_115801_add_status_report_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'name' => 'Статус отправки отчета',
            'key' => 'report_status',
            'value' => 'true',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181015_115801_add_status_report_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181015_115801_add_status_report_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
