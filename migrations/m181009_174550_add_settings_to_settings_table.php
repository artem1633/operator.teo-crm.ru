<?php

use yii\db\Migration;

/**
 * Class m181009_174550_add_settings_to_settings_table
 */
class m181009_174550_add_settings_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
           'name' => 'Комиссия водителя',
            'key' => 'percent_to_charge',
            'value' => 20,
        ]);

        $this->insert('settings', [
            'name' => 'Лимит баланса водителя',
            'key' => 'credit_limit',
            'value' => 100,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181009_174550_add_settings_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181009_174550_add_settings_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
