<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m180928_052152_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'key' => $this->string(255)->comment('Ключ'),
            'value' => $this->string(255)->comment('Значение'),
        ]);

        $this->addCommentOnTable('settings', 'Таблица настроек');

        $this->insert('settings',array(
            'name' => 'Сумма за 1 активацию',
            'key' => 'amount_for_one_activation',
            'value' => '0',
        ));

        $this->insert('settings',array(
            'name' => 'Сумма за 1 минуту',
            'key' => 'amount_in_one_minute',
            'value' => '0',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
