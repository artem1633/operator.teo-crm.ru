<?php

use yii\db\Migration;

/**
 * Class m181007_065659_change_type_invited_by_column_to_drivers_table
 */
class m181007_065659_change_type_invited_by_column_to_drivers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('drivers', 'invited_by', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181007_065659_change_type_invited_by_column_to_drivers_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181007_065659_change_type_invited_by_column_to_drivers_table cannot be reverted.\n";

        return false;
    }
    */
}
