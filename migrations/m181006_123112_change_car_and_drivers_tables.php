<?php

use yii\db\Migration;

/**
 * Class m181006_123112_change_car_and_drivers_tables
 */
class m181006_123112_change_car_and_drivers_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('drivers', 'id_api');
        $this->dropColumn('car', 'id_api');

        $this->renameColumn('car', 'mark_id', 'mark');
        $this->renameColumn('car', 'model_id', 'model');

        $this->alterColumn('car', 'mark', $this->string());
        $this->alterColumn('car', 'model', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181006_123112_change_car_and_drivers_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181006_123112_change_car_and_drivers_tables cannot be reverted.\n";

        return false;
    }
    */
}
