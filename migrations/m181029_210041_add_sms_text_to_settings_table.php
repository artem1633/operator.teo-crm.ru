<?php

use yii\db\Migration;

/**
 * Class m181029_210041_add_sms_text_to_settings_table
 */
class m181029_210041_add_sms_text_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'name' => 'Текст СМС сообщения (код подтверждения подставляется в конце сообщения)',
            'key' => 'sms_text',
            'value' => ''
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181029_210041_add_sms_text_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181029_210041_add_sms_text_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
