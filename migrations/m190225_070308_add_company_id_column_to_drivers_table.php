<?php

use yii\db\Migration;

/**
 * Handles adding company_id to table `drivers`.
 */
class m190225_070308_add_company_id_column_to_drivers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('drivers', 'company_id', $this->integer()->comment('Код компании'));

        $this->createIndex(
            'idx-drivers-company_id',
            'drivers',
            'company_id');

        $this->addForeignKey(
            'fk-drivers-company_id',
            'drivers',
            'company_id',
            'company',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
