<?php

use yii\db\Migration;

/**
 * Handles adding passport_number to table `drivers`.
 */
class m190227_164736_add_passport_number_column_to_drivers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('drivers', 'passport_number', $this->string()->comment('Серия и номер паспорта'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190227_164736_add_passport_number_column_to_drivers_table cannot be reverted.\n";

        return false;
    }
}
