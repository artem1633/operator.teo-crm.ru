<?php

use yii\db\Migration;

/**
 * Handles the creation of table `car`.
 */
class m181005_104228_create_car_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('car', [
            'id' => $this->primaryKey(),
            'color'=>$this->string()->comment('Цвет')->notNull(),
            'registration_number'=>$this->string()->comment('Регистрационный номер')->notNull(),
            'status'=>$this->string()->comment('Статус машины. A - допущен, NM - не соответствует'),
            'car_license'=>$this->string()->comment('Номер разрешения'),
            'car_license_number'=>$this->string()->comment('Номер бланка'),
            'car_date_license'=>$this->date()->comment('Дата выдачи лицензии'),
            'car_date_license_end'=>$this->date()->comment('Дата окончания лицензии'),

            'mark_id' => $this->integer()->comment('Марка автомобиля'),
            'model_id' => $this->integer()->comment('Модель автомобиля'),
            'year_manufacture' => $this->integer()->comment('Год выпуска авто'),
            'sts_number' => $this->string(255)->comment('Номер СТС'),
            'foto_sts_front_side' => $this->string(255)->comment('Фото СТС лицевая сторона'),
            'foto_sts_reverse_side' => $this->string(255)->comment('Фото СТС оборотная сторона'),
            'foto_license_front_side' => $this->string(255)->comment('Фото Лицензии лицевая сторона'),
            'foto_license_reverse_side' => $this->string(255)->comment('Фото Лицензии лицевая сторона'),
            'foto_dot_a' => $this->string(255)->comment('Фото автомобиля с точка А'),
            'foto_dot_b' => $this->string(255)->comment('Фото автомобиля с точка Б'),
            'foto_dot_v' => $this->string(255)->comment('Фото автомобиля с точка В'),
            'foto_dot_g' => $this->string(255)->comment('Фото автомобиля с точка Г'),
            'foto_salon' => $this->string(255)->comment('Фото салона'),


            'driver_id'=>$this->integer()->comment('Код водителя'),
        ]);

        $this->createIndex(
            'idx-car-driver_id',
            'car',
            'driver_id',
            false
            );
        $this->addForeignKey(
            'fk-car-driver_id',
            'car',
            'driver_id',
            'drivers',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('car');
    }
}
