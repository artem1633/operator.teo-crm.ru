<?php

use yii\db\Migration;

/**
 * Handles the creation of table `drivers`.
 */
class m180928_095759_create_drivers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('drivers', [
            'id' => $this->primaryKey(),
            'surname' => $this->string(255)->comment('Фамилия'),
            'name' => $this->string(255)->comment('Имя'),
            'middle_name' => $this->string(255)->comment('Отчество'),
            'phone' => $this->string(255)->comment('Телефон'),
            'birthday' => $this->date()->comment('Дата рождение'),
            'password_number' => $this->string(255)->comment('Номер паспорта'),
            'issued_by' => $this->string(255)->comment('Кем выдан (паспорт)'),
            'issue_date_passport' => $this->date()->comment('Дата выдачи паспорта'),
            'number_bu' => $this->string(255)->comment('Номер ВУ'),
            'class_bu' => $this->string(255)->comment('Класс ВУ'),
            'issue_date_bu' => $this->date()->comment('Дата выдачи ВУ'),
            'callsign' => $this->string(255)->comment('Позывной'),
            'foto_passport' => $this->string(255)->comment('Фото Паспорт главная страница'),
            'foto_propiska' => $this->string(255)->comment('Фото Паспорт прописка'),
            'foto_bu_front_side' => $this->string(255)->comment('Фото ВУ лицевая страница'),
            'foto_bu_reverse_side' => $this->string(255)->comment('Фото ВУ оборотная страница'),
            'foto_driver' => $this->string(255)->comment('Фото водителя'),
            'status_id' => $this->integer()->comment('Статус'),
            'creator_id' => $this->integer()->comment('Кто создал'),
            'activator_id' => $this->integer()->comment('Кто активировал'),
            'date_create' => $this->date()->comment('Дата создания'),
            'date_activation' => $this->date()->comment('Дата активации'),
            'status_api' => $this->integer()->comment('Статус Отправки по апи'), //Сделано/Ошибка/Не сделанно
            'comment' => $this->text()->comment('Комментарий '),

            'mark_id' => $this->integer()->comment('Марка автомобиля'),
            'model_id' => $this->integer()->comment('Модель автомобиля'),
            'year_manufacture' => $this->integer()->comment('Год выпуска авто'),
            'state_number' => $this->string(255)->comment('Гос. номер'),
            'sts_number' => $this->string(255)->comment('Номер СТС'),
            'resolution' => $this->string(255)->comment('Разрешение'),
            'license' => $this->string(255)->comment('Серия и № лицензии'),
            'foto_sts_front_side' => $this->string(255)->comment('Фото СТС лицевая сторона'),
            'foto_sts_reverse_side' => $this->string(255)->comment('Фото СТС оборотная сторона'),
            'foto_license_front_side' => $this->string(255)->comment('Фото Лицензии лицевая сторона'),
            'foto_license_reverse_side' => $this->string(255)->comment('Фото Лицензии лицевая сторона'),
            'foto_dot_a' => $this->string(255)->comment('Фото автомобиля с точка А'),
            'foto_dot_b' => $this->string(255)->comment('Фото автомобиля с точка Б'),
            'foto_dot_v' => $this->string(255)->comment('Фото автомобиля с точка В'),
            'foto_dot_g' => $this->string(255)->comment('Фото автомобиля с точка Г'),
            'foto_salon' => $this->string(255)->comment('Фото салона'),
        ]);

        $this->addCommentOnTable('drivers', 'Водители');

        $this->createIndex('idx-drivers-status_id', 'drivers', 'status_id', false);
        $this->addForeignKey("fk-drivers-status_id", "drivers", "status_id", "driver_status", "id");

        $this->createIndex('idx-drivers-creator_id', 'drivers', 'creator_id', false);
        $this->addForeignKey("fk-drivers-creator_id", "drivers", "creator_id", "users", "id");

        $this->createIndex('idx-drivers-activator_id', 'drivers', 'activator_id', false);
        $this->addForeignKey("fk-drivers-activator_id", "drivers", "activator_id", "users", "id");

        $this->createIndex('idx-drivers-mark_id', 'drivers', 'mark_id', false);
        $this->addForeignKey("fk-drivers-mark_id", "drivers", "mark_id", "marks", "id");

        $this->createIndex('idx-drivers-model_id', 'drivers', 'model_id', false);
        $this->addForeignKey("fk-drivers-model_id", "drivers", "model_id", "models", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-drivers-status_id','drivers');
        $this->dropIndex('idx-drivers-status_id','drivers');

        $this->dropForeignKey('fk-drivers-creator_id','drivers');
        $this->dropIndex('idx-drivers-creator_id','drivers');

        $this->dropForeignKey('fk-drivers-activator_id','drivers');
        $this->dropIndex('idx-drivers-activator_id','drivers');

        $this->dropForeignKey('fk-drivers-mark_id','drivers');
        $this->dropIndex('idx-drivers-mark_id','drivers');

        $this->dropForeignKey('fk-drivers-model_id','drivers');
        $this->dropIndex('idx-drivers-model_id','drivers');

        $this->dropTable('drivers');
    }
}
