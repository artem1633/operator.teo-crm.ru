<?php

use yii\db\Migration;

/**
 * Handles adding invited_by to table `users`.
 */
class m181010_130111_add_invited_by_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'invited_by', $this->string()->comment('ID пригласившего'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181010_130111_add_invited_by_column_to_users_table cannot be reverted.\n";

        return false;
    }
}
