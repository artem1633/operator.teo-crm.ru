<?php

use yii\db\Migration;

/**
 * Class m181014_072347_add_setting_to_settings_table
 */
class m181014_072347_add_setting_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'name' => 'Наименование группы',
            'key' => 'group_name',
            'value' => '@sityguru',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181014_072347_add_setting_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181014_072347_add_setting_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
