<?php

use yii\db\Migration;

/**
 * Class m181002_054745_add_uder_id_column_in_change_table
 */
class m181002_054745_add_uder_id_column_in_change_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('change', 'user_id', $this->integer()->comment('Код пользователя'));
        $this->createIndex('idx-change-user_id', 'change', 'user_id', false);
        $this->addForeignKey('fk-change-user_id', 'change', 'user_id', 'users', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-change-user_id', 'change');
        $this->dropColumn('change', 'user_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181002_054745_add_uder_id_column_in_change_table cannot be reverted.\n";

        return false;
    }
    */
}
