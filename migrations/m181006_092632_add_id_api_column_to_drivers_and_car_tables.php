<?php

use yii\db\Migration;

/**
 * Class m181006_092632_add_id_api_column_to_drivers_and_car_tables
 */
class m181006_092632_add_id_api_column_to_drivers_and_car_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('drivers', 'id_api', $this->string()->comment('Код водителя. Генерируется АПИ сервисом'));
        $this->addColumn('car', 'id_api', $this->string()->comment('Код автомобиля. Генерируется АПИ сервисом'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181006_092632_add_id_api_column_to_drivers_and_car_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181006_092632_add_id_api_column_to_drivers_and_car_tables cannot be reverted.\n";

        return false;
    }
    */
}
