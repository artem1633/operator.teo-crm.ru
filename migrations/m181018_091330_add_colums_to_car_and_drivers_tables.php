<?php

use yii\db\Migration;

/**
 * Class m181018_091330_add_colums_to_car_and_drivers_tables
 */
class m181018_091330_add_colums_to_car_and_drivers_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('drivers', 'driver_company_outer_id',$this->string()->comment('ID водителя для сервиса CityMobil'));
        $this->addColumn('car', 'car_company_outer_id',$this->string()->comment('ID автомобиля для сервиса CityMobil'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('car', 'car_company_outer_id');
        $this->dropColumn('drivers', 'driver_company_outer_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181018_091330_add_colums_to_car_and_drivers_tables cannot be reverted.\n";

        return false;
    }
    */
}
