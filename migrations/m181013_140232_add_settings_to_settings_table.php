<?php

use yii\db\Migration;

/**
 * Class m181013_140232_add_settings_to_settings_table
 */
class m181013_140232_add_settings_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
           'name' => 'АПИ ключ бота телеграм',
           'key' => 'telegram_api_key',
            'value' => ''
        ]);
        $this->insert('settings', [
            'name' => 'Время отчета',
            'key' => 'report_time',
            'value' => '10:00'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181013_140232_add_settings_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181013_140232_add_settings_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
