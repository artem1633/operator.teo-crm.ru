<?php

use yii\db\Migration;

/**
 * Class m181006_065201_add_api_key_setting_to_settings_table
 */
class m181006_065201_add_api_key_setting_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'name' => 'АПИ ключ',
            'key' => 'api_key',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181006_065201_add_api_key_setting_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181006_065201_add_api_key_setting_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
