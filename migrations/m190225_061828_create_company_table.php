<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m190225_061828_create_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'name' => $this->text()->comment('Наименование компании'),
            'letter' => $this->string()->comment('Буква для позывного водителя'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('company');
    }
}
