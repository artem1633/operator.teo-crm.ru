<?php

use yii\db\Migration;

/**
 * Class m181009_182516_add_columns_to_drivers_table
 */
class m181009_182516_add_columns_to_drivers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('drivers', 'need_sticker', $this->boolean()->comment('Нужна ли наклейка. True == нужна')->defaultValue(false));
        $this->addColumn('drivers', 'data_sto', $this->date()->comment('Дата выдачи СТО'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('drivers', 'need_sticker');
       $this->dropColumn('drivers', 'data_sto');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181009_182516_add_columns_to_drivers_table cannot be reverted.\n";

        return false;
    }
    */
}
