<?php

use yii\db\Migration;

/**
 * Class m181002_192857_change_type_average_speed_column_to_change_table
 */
class m181002_192857_change_type_average_speed_column_to_change_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('change', 'average_speed', $this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181002_192857_change_type_average_speed_column_to_change_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181002_192857_change_type_average_speed_column_to_change_table cannot be reverted.\n";

        return false;
    }
    */
}
