<?php

use yii\db\Migration;

/**
 * Class m181006_151726_add_status_to_driver_status_table
 */
class m181006_151726_add_status_to_driver_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('driver_status',[
            'name' => 'На исправлении',
            'color' => '#ffbd88',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181006_151726_add_status_to_driver_status_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181006_151726_add_status_to_driver_status_table cannot be reverted.\n";

        return false;
    }
    */
}
