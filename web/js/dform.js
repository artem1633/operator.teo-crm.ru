var info = $('.confirm-info');
var btn = $('#confirm-btn');
var progress = $('#confirm-progress');
var head = $('#modal-head');
var input_code = $('#confirm-code');


function clear_confirm() {
    head.addClass('hidden');
    btn.attr('disabled', false);
    show(btn);
    info.addClass('hidden');
    input_code.addClass('hidden');
    progress.addClass('hidden');

}

function send_sms() {
    var phone = $('#drivers-phone').val();
    if (phone == '') {
        btn.attr('disabled', true);
        show(info, 'Ошибка. Не указан номер телефона! Укажите номер телефона и повторите попытку.');
        info.addClass('alert-warning');
        return;
    }

    btn.addClass('hidden');
    progress.removeClass('hidden');
    show(head, 'Производится отправка сообщения...');

    $.ajax({
        url: 'sendsms',
        type: 'POST',
        data: 'phone=' + phone,
        success: function (data) {
            if (data == 'ok') {
                show(info, 'SMS-сообщение с кодом подтверждения успешно отправлено');
                info.addClass('alert-success');
                //Показываем инпут для кода
                show(input_code);
                hide(info, 3000);
                hide(head);
                hide(progress);
            } else {
                info.removeClassWild('alert-*');
                info.addClass('alert-danger');
                show(info, 'Ошибка отправки сообщения - ' + data);
                hide(progress);
                hide(head);
            }
        },
        error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            $('#get-users').addClass('alert-danger');
            $('#get-users-group-info').text(msg);
            $('#getUsersBtn').removeClass('disabled');
        }

    })
}

function checkCode() {
    var code = $('#input-code').val();
    if (code == '') {
        show(info, 'Введите код!');
        info.removeClassWild('alert-*');
        info.addClass('alert-warning');
        hide(info, 3000);
        return;
    }

    $.ajax({
        url: 'getcode',
        type: 'POST',
        success: function (data) {
            if (data != '') {
                if (code == data) {
                    input_code.addClass('hidden');
                    //Нажимаем на кнопку сохранения
                    $('#submit_btn').click();
                } else {
                    info.removeClassWild('alert-*');
                    info.addClass('alert-danger');
                    show(info, 'Неверный код, попробуйте еще раз');
                    hide(info, 3000);

                }
            }
        }
    })
}

function saveNotConfirm() {
//Передаем, что акк водителя не подтвержден
    $.ajax({
        url: 'create-no-verify',
        type: 'POST',
        success: function () {
            $('#submit_btn').click();
        }
    })
}

function show(selector, text) {

    selector.removeClass('hidden');
    selector.show('slow');
    selector.attr('disabled', false);
    if (text) {
        selector.text(text);
    }
}

function hide(selector, delay) {
    if (delay > 0) {
        setTimeout(function () {
            selector.addClass('hidden');
        }, delay);
    } else {
        selector.addClass('hidden');
    }

}

$.fn.removeClassWild = function (mask) {
    return this.removeClass(function (index, cls) {
        var re = mask.replace(/\*/g, '\\S+');
        return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
    });
};

$('#comment').keyup(function () {
    var somecomment = $("#comment").val();
    var reject_btn =  $("#reject-btn");
    var correct_btn =  $("#correct-btn");

    if (somecomment != '') {
        reject_btn.attr("onclick", "rejectActivation()");
        reject_btn.attr("disabled", false);
        correct_btn.attr("onclick", "correct()");
        correct_btn.attr("disabled", false);
    } else {
        reject_btn.attr("onclick", "return false");
        reject_btn.attr("disabled", true);
        correct_btn.attr("onclick", "return false");
        correct_btn.attr("disabled", true);
    }
});

$('#accept-btn, #reject-btn, #correct-btn').click(function() {
    var comment = $('#comment').val();
    window.location = $(this).attr('data-href') + '&comment=' + comment;
});

// $('#reject-btn').click(function() {
//     var comment = $('#comment').val();
//     var new_url = $(this).attr('data-href') + '&comment=' + comment;
//     window.location = new_url;
// });
//
// $('#correct-btn').click(function() {
//     var comment = $('#comment').val();
//     var new_url = $(this).attr('data-href') + '&comment=' + comment;
//     window.location = new_url;
// });


// function correct() {
//     var comment = $('#comment').val();
//     var id = $('#id-driver').text();
//     $.ajax({
//         url: 'correct',
//         type: 'POST',
//         data: {
//             id: id,
//             comment: comment
//         },
//         success: function () {
//             window.location = 'index';
//         }
//     });
// }

