<?php

namespace app\modules\api\controllers;

use app\models\Change;
use app\models\Settings;
use yii\base\Exception;
use yii\helpers\Json;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class ClientController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['sendreport', 'pushtelegram', 'send', 'status', 'history', 'proxy', 'test', 'akkunread', 'getdialog', 'getbotdialog', 'akkread'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    /**
     * Отправка в телеграм канал
     *
     * @param $text
     * @param bool $is_activation
     * @return mixed
     */
    public static function actionPushtelegram($text, $is_activation = false)
    {
        if (!$is_activation) {
            $text = urlencode($text);
        }
//        $user_id = '@sityguru';
        $user_id = Settings::getSettingValueByKey('group_name');
        $proxy_ip = Settings::getSettingValueByKey('ip_proxy');
        $proxy_port = Settings::getSettingValueByKey('port_proxy');
        $proxy = $proxy_ip . ":" . $proxy_port;
        $token = Settings::getSettingValueByKey('telegram_api_key');
        $url = 'https://api.telegram.org/bot' . $token . '/sendMessage?chat_id=' . $user_id . '&disable_web_page_preview=true&text=' . $text;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        Yii::warning('Прокси: ' . $proxy);
        Yii::warning('URL: ' . $url);
        Yii::warning($curl_scraped_page);

//        VarDumper::dump($curl_scraped_page, 10, true);

        return $curl_scraped_page;
    }

    public function actionSendReport()
    {
        $model = Settings::find()->where(['key' => 'report_status'])->one();
        $report_status = Settings::getSettingValueByKey('report_status'); //Статус отправки отчета
        $report_time = strtotime(date('Y-m-d ') . Settings::getSettingValueByKey('report_time') . ':00'); //Дата и Время отчета
//        $current_time = strtotime(date('Y-m-d H:i:s')); //Текущее время
        $current_time = time(); //Текущее время

        if ($current_time < $report_time && $report_status == 'true') { //Если текущее время меньше отчетного и статус отправки отчета = отправлен (true)
            $model->value = 'false'; //Статус отправки отчета
            $model->save();
        } elseif ($current_time > $report_time && $report_status == 'false') { //Если текущее время больше отчетного и статус отправки = не отправлен (false)
            $report = Change::getReport(); //Создаем отчет
            Yii::warning('Отчет: ' . $report, __METHOD__);
            self::actionPushtelegram($report); //Отправляем
            $model->value = 'true'; //Меняем статус отправки
            $model->save();
        }

        Yii::warning(Date(DATE_ATOM, $report_time), __METHOD__);
        Yii::warning(Date(DATE_ATOM, $current_time), __METHOD__);


        return 'ok';
    }

    /**
     * @param string $method Метод используемый в вызове
     * @param array $params Массив передаваемых параметров
     * @param string $send_method Post или Get
     * @return bool|mixed|string
     * @throws Exception
     */
    public static function requestAPI($method, $params, $send_method = 'POST')
    {
        $version = '1.0.0';
        $auth_key = Settings::find()->where(['key' => 'api_key'])->one()->value ?? null;
        $url = "https://partner-api.city-mobil.ru/{$version}/{$method}";

        if ($send_method == 'GET') {
            $url .= "?auth_key={$auth_key}";
            //Собираем ссылку из параметров
            foreach ($params as $key => $value) {
                $url .= '&' . $key . '=' . $value;
            }
        } else {
            $params['auth_key'] = $auth_key;
        }

        Yii::info('URL адрес: ' . $url, 'test');
        Yii::info('Параметры', 'test');
        Yii::info($params, 'test');

        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'request_fulluri' => true,
                'method' => $send_method,
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

        Yii::info('Результат запроса', 'test');

        if ($result == false){ //При неработающем прокси $result == false
            throw new Exception('Ошибка. Проверьте прокси');
//            return 0;
        } else {
            $result = Json::decode($result);
        }

        Yii::info($result, 'test');


//            //Проверка ответа
//        if ($method == 'createOrUpdateDriver') {
//
//            Yii::warning($result);
//
//            if ($result['success' == 0]){
////                return 'Ошибка. ' . $result['data']['msg'];
//                return 0;
//            } else {
////                return 'Успешно отправлено';
//                return 1;
//            }
//
//        }
            return $result;
    }
}
