<?php

namespace app\controllers;

use app\models\Car;
use app\models\Change;
use app\models\Settings;
use app\models\User;
use app\models\Users;
use app\modules\api\controllers\ClientController;
use Yii;
use app\models\Drivers;
use app\models\DriversSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use ZipArchive;

/**
 * DriversController implements the CRUD actions for Drivers model.
 */
class DriversController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Drivers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DriversSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Users::isAgent()) {
            $dataProvider = new ActiveDataProvider([
                'query' => Drivers::find()->where(['creator_id' => Yii::$app->user->id])->orderBy(['date_create' => SORT_DESC, 'id' => SORT_DESC]),
            ]);
        } elseif (Users::isActivator()) {
            $dataProvider = new ActiveDataProvider([
                'query' => Drivers::find()->where(['status_id' => Drivers::DRIVER_STATUS_FREE])->orWhere(['activator_id' => Yii::$app->user->id])->orderBy(['date_create' => SORT_DESC, 'id' => SORT_DESC]),//->orderBy(['date_activation' => SORT_DESC, 'id' => SORT_DESC]),
            ]);
        }


        $provider = null;
        if (Yii::$app->request->get('show_expected')) {

            $provider = new ActiveDataProvider([
                'query' => Drivers::find()->where(['status_id' => Drivers::DRIVER_STATUS_ACTIVATED])->andWhere(['creator_id' => Yii::$app->user->id]),
            ]);


            $query = Drivers::find()->where(['status_id' => Drivers::DRIVER_STATUS_ACTIVATED]); //Водители со статусом активирован

            $activatedDrivers = $query->orderBy('date_activation');
            Yii::info($activatedDrivers, __METHOD__);

        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'expectedProvider' => $provider
        ]);
    }


    /**
     * Displays a single Drivers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $car_model = Car::findOne(['driver_id' => $id]);

        $request = Yii::$app->request;
        if ($request->isAjax) {
//            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Данные о водителе: " . Drivers::getShortName($this->id),
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                    'car_model' => $car_model
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $model,
                'car_model' => $car_model
            ]);
        }


    }

    /**
     * Creates a new Drivers model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $session = Yii::$app->session;

        $noVerify = Yii::$app->session->get('noVerify');
        $session->set('noVerify', false);

        $request = Yii::$app->request;

        $model = new Drivers;
        $car_model = new Car;

        If ($request->isAjax) {
            $model->creator_id = Yii::$app->user->id;
            //Если при создании водителя оне не прошел процедуру подтверждения
            // ему присваиваетя статус - Не подтвержден, в противном случае статус - Подтвержден
            if ($noVerify) {
                $model->status_id = Drivers::DRIVER_STATUS_NOT_CONFIRMED;
            } else {
                $model->status_id = Drivers::DRIVER_STATUS_FREE;
            }


            if ($car_model->load($request->post()) && $model->load($request->post()) && $car_model->save() && $model->save()) {

                Yii::info('Статус водителя: ' . $model->status_id, 'test');

                $model->callsign = Drivers::getCallSign($model->id);
                $model->driver_status = 'D'; //Статус - Черновик

                $car_model->driver_id = $model->id;
                $car_model->save();

                Yii::info('Код водителя в car: ' . $car_model->driver_id, 'test');
                Yii::info('Код авто: ' . $car_model->id, 'test');

//                if ($model->status_id != Drivers::DRIVER_STATUS_NOT_CONFIRMED) { //Если водитель подтвержден по СМС
//
//                    С марта 2019 года НЕ Создаем пользователя в сторонней систиеме
//
//                    Yii::info('Код водителя перед отправкой: ' . $car_model->driver_id, 'test');
//
//                    //Формируем массив параметров
//                    $params = Drivers::prepareParams($model);
//
//                    //Добавляем водителя и авто (createOrUpdateDriver (POST)
//                    $result = ClientController::requestAPI('createOrUpdateDriver', $params);
//
//                    if ($result['data']['driver_company_outer_id']) {
//
//                        $model->driver_company_outer_id = $result['data']['driver_company_outer_id'];
//                        $car_model->car_company_outer_id = $result['data']['car_company_outer_id'];
//
//                        $model->status_api = 1;
//
//                        Yii::info($model->driver_company_outer_id, 'test');
//                        Yii::info($car_model->car_company_outer_id, 'test');
//                    } else {
//                        Yii::$app->session->setFlash('error', $result['type']);
//                        $model->status_api = 0;
//                    }
//
//                    Yii::info($result, __METHOD__);
//
//                }

                $model->save();
                $car_model->save();

//            Путь сохранения файла: uploads/Код_водителя/
                $path = 'uploads/' . $model->id . '/';

                if (!file_exists($path)) {
                    if (!mkdir($path, 0755, true)) {
                        Yii::info('Путь: ' . $path . ' папка не создана!', __METHOD__);
                    } else {
                        Yii::info('Путь: ' . $path . ' папка создана!', __METHOD__);
                    };
                }

                $model->file_foto_passport = UploadedFile::getInstance($model, 'file_foto_passport');
                if (!empty($model->file_foto_passport)) {
                    Yii::info($model->file_foto_passport->baseName, __METHOD__);
                    $model->file_foto_passport->saveAs($path . 'passport.' . $model->file_foto_passport->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_passport' => 'passport.' . $model->file_foto_passport->extension], ['id' => $model->id])->execute();
                }

                $model->file_foto_propiska = UploadedFile::getInstance($model, 'file_foto_propiska');
                if (!empty($model->file_foto_propiska)) {
                    $model->file_foto_propiska->saveAs($path . 'propiska.' . $model->file_foto_propiska->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_propiska' => 'propiska.' . $model->file_foto_propiska->extension], ['id' => $model->id])->execute();
                }

                $model->file_foto_bu_front_side = UploadedFile::getInstance($model, 'file_foto_bu_front_side');
                if (!empty($model->file_foto_bu_front_side)) {
                    $model->file_foto_bu_front_side->saveAs($path . 'front_side.' . $model->file_foto_bu_front_side->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_bu_front_side' => 'front_side.' . $model->file_foto_bu_front_side->extension], ['id' => $model->id])->execute();
                }

                $model->file_foto_bu_reverse_side = UploadedFile::getInstance($model, 'file_foto_bu_reverse_side');
                if (!empty($model->file_foto_bu_reverse_side)) {
                    $model->file_foto_bu_reverse_side->saveAs($path . 'reverse_side.' . $model->file_foto_bu_reverse_side->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_bu_reverse_side' => 'reverse_side.' . $model->file_foto_bu_reverse_side->extension], ['id' => $model->id])->execute();
                }

                $model->file_foto_driver = UploadedFile::getInstance($model, 'file_foto_driver');
                if (!empty($model->file_foto_driver)) {
                    $model->file_foto_driver->saveAs($path . 'driver.' . $model->file_foto_driver->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_driver' => 'driver.' . $model->file_foto_driver->extension], ['id' => $model->id])->execute();
                }

                $model->foto_a = UploadedFile::getInstance($model, 'foto_a');
                if (!empty($model->foto_a)) {
                    $model->foto_a->saveAs($path . 'a.' . $model->foto_a->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_dot_a' => 'a.' . $model->foto_a->extension], ['id' => $model->id])->execute();
                }

                $model->foto_b = UploadedFile::getInstance($model, 'foto_b');
                if (!empty($model->foto_b)) {
                    $model->foto_b->saveAs($path . 'b.' . $model->foto_b->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_dot_b' => 'b.' . $model->foto_b->extension], ['id' => $model->id])->execute();
                }

                $model->foto_v = UploadedFile::getInstance($model, 'foto_v');
                if (!empty($model->foto_v)) {
                    $model->foto_v->saveAs($path . 'v.' . $model->foto_v->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_dot_v' => 'v.' . $model->foto_v->extension], ['id' => $model->id])->execute();
                }

                $model->foto_g = UploadedFile::getInstance($model, 'foto_g');
                if (!empty($model->foto_g)) {
                    $model->foto_g->saveAs($path . 'g.' . $model->foto_g->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_dot_g' => 'g.' . $model->foto_g->extension], ['id' => $model->id])->execute();
                }

                $model->file_foto_salon = UploadedFile::getInstance($model, 'file_foto_salon');
                if (!empty($model->file_foto_salon)) {
                    $model->file_foto_salon->saveAs($path . 'salon.' . $model->file_foto_salon->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_salon' => 'salon.' . $model->file_foto_salon->extension], ['id' => $model->id])->execute();
                }

                $model->foto_license_front = UploadedFile::getInstance($model, 'foto_license_front');
                if (!empty($model->foto_license_front)) {
                    $model->foto_license_front->saveAs($path . 'license_front.' . $model->foto_license_front->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_license_front_side' => 'license_front.' . $model->foto_license_front->extension], ['id' => $model->id])->execute();
                }

                $model->foto_license_reverse = UploadedFile::getInstance($model, 'foto_license_reverse');
                if (!empty($model->foto_license_reverse)) {
                    $model->foto_license_reverse->saveAs($path . 'license_reverse.' . $model->foto_license_reverse->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_license_reverse_side' => 'license_reverse.' . $model->foto_license_reverse->extension], ['id' => $model->id])->execute();
                }

                $model->foto_sts_front = UploadedFile::getInstance($model, 'foto_sts_front');
                if (!empty($model->foto_sts_front)) {
                    $model->foto_sts_front->saveAs($path . 'sts_front.' . $model->foto_sts_front->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_sts_front_side' => 'sts_front.' . $model->foto_sts_front->extension], ['id' => $model->id])->execute();
                }

                $model->foto_sts_reverse = UploadedFile::getInstance($model, 'foto_sts_reverse');
                if (!empty($model->foto_sts_reverse)) {
                    $model->foto_sts_reverse->saveAs($path . 'sts_reverse.' . $model->foto_sts_reverse->extension);
                    Yii::$app->db->createCommand()->update('drivers', ['foto_sts_reverse_side' => 'sts_reverse.' . $model->foto_sts_reverse->extension], ['id' => $model->id])->execute();
                }

                return $this->redirect(['index']);
            } else {
                $car_model = new Car;
                return $this->render('create', [
                    'model' => $model,
                    'car_model' => $car_model,
                ]);
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($car_model->load($request->post()) && $model->load($request->post()) && $car_model->save() && $model->save()) {

                if ($noVerify) {
                    $model->status_id = Drivers::DRIVER_STATUS_NOT_CONFIRMED; //Статус - Не подтвержден
                } else {
                    $model->status_id = Drivers::DRIVER_STATUS_FREE; //Статус - свободен
                }

                $model->callsign = Drivers::getCallSign($model->id);
                $model->driver_status = 'D'; //Статус - Черновик

                $car_model->driver_id = $model->id;
                $car_model->save();

                Yii::info('Код водителя в car: ' . $car_model->driver_id);
                Yii::info('Код авто: ' . $car_model->id);

//                if ($model->status_id != Drivers::DRIVER_STATUS_NOT_CONFIRMED) { //Если водитель подтвержден по СМС
//
//                    //Создаем пользователя в сторонней систиеме
//
//                    Yii::info('Код водителя перед отправкой: ' . $car_model->driver_id);
//
//                    //Формируем массив параметров
//                    $params = Drivers::prepareParams($model);
//
//                    //Добавляем водителя и авто (createOrUpdateDriver (POST)
//                    $result = ClientController::requestAPI('createOrUpdateDriver', $params);
//
//                    if ($result['data']['driver_company_outer_id']) {
//
//                        $model->driver_company_outer_id = $result['data']['driver_company_outer_id'];
//                        $car_model->car_company_outer_id = $result['data']['car_company_outer_id'];
//
//                        $model->status_api = 1;
//
//                        Yii::info($model->driver_company_outer_id, __METHOD__);
//                        Yii::info($car_model->car_company_outer_id, __METHOD__);
//                    } else {
//                        Yii::$app->session->setFlash('error', $result['type']);
//                        $model->status_api = 0;
//                    }
//
//                    Yii::info($result, __METHOD__);
//
//                }
                $temp_folder = $session->get('tmpFolder');

                Yii::info('Временная папка: ' . $temp_folder, 'test');

                if ($temp_folder) {
                    //Перемещаем фотки
                    if (!rename('uploads/' . $temp_folder, 'uploads/' . $model->id)) {
                        Yii::error('Ошибка перемещения фото', __METHOD__);
                        Yii::$app->session->setFlash('error', 'Ошибка перемещения фото');
                    } else {
                        $session->remove('tmpFolder');
                    }

                }

                $model->save();
                $car_model->save();

//            Путь сохранения файла: uploads/Код_водителя/
                $path = 'uploads/' . $model->id . '/';

                if (!file_exists($path)) {
                    if (!mkdir($path, 0755, true)) {
                        Yii::info('Путь: ' . $path . ' папка не создана!', __METHOD__);
                    } else {
                        Yii::info('Путь: ' . $path . ' папка создана!', __METHOD__);
                    };
                }

//                $model->file_foto_passport = UploadedFile::getInstance($model, 'file_foto_passport');
//                if (!empty($model->file_foto_passport)) {
//                    Yii::info($model->file_foto_passport->baseName, __METHOD__);
//                    $model->file_foto_passport->saveAs($path . 'passport.' . $model->file_foto_passport->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_passport' => 'passport.' . $model->file_foto_passport->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->file_foto_propiska = UploadedFile::getInstance($model, 'file_foto_propiska');
//                if (!empty($model->file_foto_propiska)) {
//                    $model->file_foto_propiska->saveAs($path . 'propiska.' . $model->file_foto_propiska->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_propiska' => 'propiska.' . $model->file_foto_propiska->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->file_foto_bu_front_side = UploadedFile::getInstance($model, 'file_foto_bu_front_side');
//                if (!empty($model->file_foto_bu_front_side)) {
//                    $model->file_foto_bu_front_side->saveAs($path . 'front_side.' . $model->file_foto_bu_front_side->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_bu_front_side' => 'front_side.' . $model->file_foto_bu_front_side->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->file_foto_bu_reverse_side = UploadedFile::getInstance($model, 'file_foto_bu_reverse_side');
//                if (!empty($model->file_foto_bu_reverse_side)) {
//                    $model->file_foto_bu_reverse_side->saveAs($path . 'reverse_side.' . $model->file_foto_bu_reverse_side->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_bu_reverse_side' => 'reverse_side.' . $model->file_foto_bu_reverse_side->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->file_foto_driver = UploadedFile::getInstance($model, 'file_foto_driver');
//                if (!empty($model->file_foto_driver)) {
//                    $model->file_foto_driver->saveAs($path . 'driver.' . $model->file_foto_driver->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_driver' => 'driver.' . $model->file_foto_driver->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->foto_a = UploadedFile::getInstance($model, 'foto_a');
//                if (!empty($model->foto_a)) {
//                    $model->foto_a->saveAs($path . 'a.' . $model->foto_a->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_dot_a' => 'a.' . $model->foto_a->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->foto_b = UploadedFile::getInstance($model, 'foto_b');
//                if (!empty($model->foto_b)) {
//                    $model->foto_b->saveAs($path . 'b.' . $model->foto_b->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_dot_b' => 'b.' . $model->foto_b->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->foto_v = UploadedFile::getInstance($model, 'foto_v');
//                if (!empty($model->foto_v)) {
//                    $model->foto_v->saveAs($path . 'v.' . $model->foto_v->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_dot_v' => 'v.' . $model->foto_v->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->foto_g = UploadedFile::getInstance($model, 'foto_g');
//                if (!empty($model->foto_g)) {
//                    $model->foto_g->saveAs($path . 'g.' . $model->foto_g->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_dot_g' => 'g.' . $model->foto_g->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->file_foto_salon = UploadedFile::getInstance($model, 'file_foto_salon');
//                if (!empty($model->file_foto_salon)) {
//                    $model->file_foto_salon->saveAs($path . 'salon.' . $model->file_foto_salon->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_salon' => 'salon.' . $model->file_foto_salon->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->foto_license_front = UploadedFile::getInstance($model, 'foto_license_front');
//                if (!empty($model->foto_license_front)) {
//                    $model->foto_license_front->saveAs($path . 'license_front.' . $model->foto_license_front->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_license_front_side' => 'license_front.' . $model->foto_license_front->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->foto_license_reverse = UploadedFile::getInstance($model, 'foto_license_reverse');
//                if (!empty($model->foto_license_reverse)) {
//                    $model->foto_license_reverse->saveAs($path . 'license_reverse.' . $model->foto_license_reverse->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_license_reverse_side' => 'license_reverse.' . $model->foto_license_reverse->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->foto_sts_front = UploadedFile::getInstance($model, 'foto_sts_front');
//                if (!empty($model->foto_sts_front)) {
//                    $model->foto_sts_front->saveAs($path . 'sts_front.' . $model->foto_sts_front->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_sts_front_side' => 'sts_front.' . $model->foto_sts_front->extension], ['id' => $model->id])->execute();
//                }
//
//                $model->foto_sts_reverse = UploadedFile::getInstance($model, 'foto_sts_reverse');
//                if (!empty($model->foto_sts_reverse)) {
//                    $model->foto_sts_reverse->saveAs($path . 'sts_reverse.' . $model->foto_sts_reverse->extension);
//                    Yii::$app->db->createCommand()->update('drivers', ['foto_sts_reverse_side' => 'sts_reverse.' . $model->foto_sts_reverse->extension], ['id' => $model->id])->execute();
//                }
                return $this->redirect(['index']);
            } else {
                $car_model = new Car;

                return $this->render('create', [
                    'model' => $model,
                    'car_model' => $car_model,
                ]);
            }
        }


    }

    /**
     * @return string|Response
     */
    public function actionCreateNoVerify()
    {
        Yii::$app->session->set('noVerify', true);
        return 'ok';


    }

    /**
     * Updates an existing Drivers model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        Yii::info('ID: ' . $id, __METHOD__);

        $request = Yii::$app->request;

        $model = $this->findModel($id);
        $car_model = Car::find()->where(['driver_id' => $id])->one();

        Yii::info('outer Driver ID: ' . $model->driver_company_outer_id, __METHOD__);
        Yii::info('outer Car ID: ' . $car_model->car_company_outer_id, __METHOD__);


        $noVerify = Yii::$app->session->get('noVerify');
        Yii::$app->session->set('noVerify', false);

        if ($model->load($request->post()) && $car_model->load($request->post()) && $model->save() && $car_model->save()) {
//            VarDumper::dump($car_model, 10 , true);
            if ($noVerify) { //Если не прошел верификацию по смс
                $model->status_id = Drivers::DRIVER_STATUS_NOT_CONFIRMED;
            } elseif ($model->status_id = Drivers::DRIVER_STATUS_NOT_CONFIRMED) { //Если не прошел верификацию и уже стоит статус "Не прошел верификацию"
                $model->status_id = Drivers::DRIVER_STATUS_FREE;
            }
            //Пересоздаем позывной (на случай изменеия телефона или ФИО)
            $model->callsign = Drivers::getCallSign($model->id);

            if ($model->status_id != Drivers::DRIVER_STATUS_NOT_CONFIRMED) { //Если водитель подтвержден по СМС

                //Создаем пользователя в сторонней систиеме

                //Формируем массив параметров
                $params = Drivers::prepareParams($model);

                //Добавляем водителя и авто (createOrUpdateDriver (POST)
                $result = ClientController::requestAPI('createOrUpdateDriver', $params);

                if (!$model->driver_company_outer_id && $result['data']['driver_company_outer_id']) {

                    $model->driver_company_outer_id = $result['data']['driver_company_outer_id'];
                    $car_model->car_company_outer_id = $result['data']['car_company_outer_id'];

                    $model->status_api = 1;
                } else {
                    //Выводим тип ошибки
                    Yii::$app->session->setFlash('error', $result['type']);
                    $model->status_api = 0;
                }

                Yii::info($result, __METHOD__);

            }

            $model->save();
            $car_model->save();

//            Путь сохранения файла: uploads/Код_водителя/
            $path = 'uploads/' . $model->id . '/';

            if (!file_exists($path)) {
                if (!mkdir($path, 0755, true)) {
                    Yii::info('Путь: ' . $path . ' папка не создана!', __METHOD__);
                } else {
                    Yii::info('Путь: ' . $path . ' папка создана!', __METHOD__);
                };
            }

            $model->file_foto_passport = UploadedFile::getInstance($model, 'file_foto_passport');
            if (!empty($model->file_foto_passport)) {
                Yii::info($model->file_foto_passport->baseName, __METHOD__);
                $model->file_foto_passport->saveAs($path . 'passport.' . $model->file_foto_passport->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_passport' => 'passport.' . $model->file_foto_passport->extension], ['id' => $model->id])->execute();
            }

            $model->file_foto_propiska = UploadedFile::getInstance($model, 'file_foto_propiska');
            if (!empty($model->file_foto_propiska)) {
                $model->file_foto_propiska->saveAs($path . 'propiska.' . $model->file_foto_propiska->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_propiska' => 'propiska.' . $model->file_foto_propiska->extension], ['id' => $model->id])->execute();
            }

            $model->file_foto_bu_front_side = UploadedFile::getInstance($model, 'file_foto_bu_front_side');
            if (!empty($model->file_foto_bu_front_side)) {
                $model->file_foto_bu_front_side->saveAs($path . 'bu_front_side.' . $model->file_foto_bu_front_side->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_bu_front_side' => 'bu_front_side.' . $model->file_foto_bu_front_side->extension], ['id' => $model->id])->execute();
            }

            $model->file_foto_bu_reverse_side = UploadedFile::getInstance($model, 'file_foto_bu_reverse_side');
            if (!empty($model->file_foto_bu_reverse_side)) {
                $model->file_foto_bu_reverse_side->saveAs($path . 'bu_reverse_side.' . $model->file_foto_bu_reverse_side->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_bu_reverse_side' => 'bu_reverse_side.' . $model->file_foto_bu_reverse_side->extension], ['id' => $model->id])->execute();
            }

            $model->file_foto_driver = UploadedFile::getInstance($model, 'file_foto_driver');
            if (!empty($model->file_foto_driver)) {
                $model->file_foto_driver->saveAs($path . 'driver.' . $model->file_foto_driver->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_driver' => 'driver.' . $model->file_foto_driver->extension], ['id' => $model->id])->execute();
            }

            $model->foto_a = UploadedFile::getInstance($model, 'foto_a');
            if (!empty($model->foto_a)) {
                $model->foto_a->saveAs($path . 'a.' . $model->foto_a->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_dot_a' => 'a.' . $model->foto_a->extension], ['id' => $model->id])->execute();
            }

            $model->foto_b = UploadedFile::getInstance($model, 'foto_b');
            if (!empty($model->foto_b)) {
                $model->foto_b->saveAs($path . 'b.' . $model->foto_b->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_dot_b' => 'b.' . $model->foto_b->extension], ['id' => $model->id])->execute();
            }

            $model->foto_v = UploadedFile::getInstance($model, 'foto_v');
            if (!empty($model->foto_v)) {
                $model->foto_v->saveAs($path . 'v.' . $model->foto_v->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_dot_v' => 'v.' . $model->foto_v->extension], ['id' => $model->id])->execute();
            }

            $model->foto_g = UploadedFile::getInstance($model, 'foto_g');
            if (!empty($model->foto_g)) {
                $model->foto_g->saveAs($path . 'g.' . $model->foto_g->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_dot_g' => 'g.' . $model->foto_g->extension], ['id' => $model->id])->execute();
            }

            $model->file_foto_salon = UploadedFile::getInstance($model, 'file_foto_salon');
            if (!empty($model->file_foto_salon)) {
                $model->file_foto_salon->saveAs($path . 'salon.' . $model->file_foto_salon->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_salon' => 'salon.' . $model->file_foto_salon->extension], ['id' => $model->id])->execute();
            }

            $model->foto_license_front = UploadedFile::getInstance($model, 'foto_license_front');
            if (!empty($model->foto_license_front)) {
                $model->foto_license_front->saveAs($path . 'license_front.' . $model->foto_license_front->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_license_front_side' => 'license_front.' . $model->foto_license_front->extension], ['id' => $model->id])->execute();
            }

            $model->foto_license_reverse = UploadedFile::getInstance($model, 'foto_license_reverse');
            if (!empty($model->foto_license_reverse)) {
                $model->foto_license_reverse->saveAs($path . 'license_reverse.' . $model->foto_license_reverse->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_license_reverse_side' => 'license_reverse.' . $model->foto_license_reverse->extension], ['id' => $model->id])->execute();
            }

            $model->foto_sts_front = UploadedFile::getInstance($model, 'foto_sts_front');
            if (!empty($model->foto_sts_front)) {
                $model->foto_sts_front->saveAs($path . 'sts_front.' . $model->foto_sts_front->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_sts_front_side' => 'sts_front.' . $model->foto_sts_front->extension], ['id' => $model->id])->execute();
            }

            $model->foto_sts_reverse = UploadedFile::getInstance($model, 'foto_sts_reverse');
            if (!empty($model->foto_sts_reverse)) {
                $model->foto_sts_reverse->saveAs($path . 'sts_reverse.' . $model->foto_sts_reverse->extension);
                Yii::$app->db->createCommand()->update('drivers', ['foto_sts_reverse_side' => 'sts_reverse.' . $model->foto_sts_reverse->extension], ['id' => $model->id])->execute();
            }

            return $this->redirect(['index']);
        } else {
//            VarDumper::dump($car_model, 10 , true);
            return $this->render('update', [
                'model' => $model,
                'car_model' => $car_model
            ]);
        }
    }

    /**
     * Delete an existing Drivers model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if ($this->findModel($id)->delete()) {
            Drivers::removeDirectory('uploads/' . $id); //Удаляем директорию водителя (с фотографиями)
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Drivers model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Drivers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Drivers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Drivers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return string
     */
    public function actionSendsms()
    {
        $phone = preg_replace('/[^0-9]/', '', Yii::$app->request->post('phone'));

        $phone = mb_substr($phone, strlen($phone) - 10);

        $code = rand(1000, 9999);
        Yii::info($code, __METHOD__);

        Yii::$app->session->set('code', $code);
//      $result = 'o4564k';
        $result = $this->requestSMS('+7' . $phone, Settings::getSettingValueByKey('sms_text') . ' ' . $code);

        if (mb_strpos($result, 'cepted;')) {
            return 'ok';
        } else {
            return $result;
        }
    }

    /**
     * @return mixed
     */
    public function actionGetcode()
    {
        return Yii::$app->session->get('code');
    }

    /**
     * @param $phone_number
     * @param $message
     * @return bool|string
     */
    protected function requestSMS($phone_number, $message)
    {
        $url = 'http://api.iqsms.ru/messages/v2/send/';

        $params['login'] = Settings::getSettingValueByKey('sms_gate_login');
        $params['password'] = Settings::getSettingValueByKey('sms_gate_password');
        $params['phone'] = $phone_number;
        $params['text'] = $message;
        $params['flash'] = 1;

        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

        return $result;

    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionTakeOnActivation($id)
    {
        Yii::info('ID: ' . $id, __METHOD__);

        $model = $this->findModel($id);

        $model->status_id = Drivers::DRIVER_STATUS_TO_ACTIVATION;
        $model->activator_id = Yii::$app->user->id;

        Yii::info('activator-ID: ' . $model->activator_id, __METHOD__);

        $model->save();
        $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Активация водителя
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @internal param $id
     */
    public function actionAcceptActivation()
    {
        $request = Yii::$app->request;
        $comment = '';
        $id = '';

        if ($request->isPost) {
            $id = trim($request->post('id'));
            $comment = $request->post('comment');
        } elseif ($request->isGet) {
            $id = $request->get('id');
            $comment = $request->get('comment');
        }

        Yii::info('ID: ' . $id, 'test');
        Yii::info('Comment: ' . $comment, 'test');

        $model = $this->findModel($id);

        //Добавляем водителя в Ситимобил
        $result = Drivers::addDriverToCityMobil($model);
        if (!$result[0]) {

            Yii::error($result[1], __METHOD__);

            $model->status_api = 0;
            $model->save();

            Yii::$app->session->setFlash('error', 'Ошибка активации. Невозможно добавить водителя в Ситимобил. ' . $result[1]);

            return $this->redirect(['update', 'id' => $model->id]);
//            return Json::encode($result);
        };

        // $model->driver_status = 'A'; //Допущен
        $model->comment = $comment;
        $model->status_id = Drivers::DRIVER_STATUS_ACTIVATED; //Активирован
        $model->date_activation = date('Y-m-d', time());
        $model->status_api = 1;

        //Получаем незавершенную смену активатора
        $changeID = Change::find()->where(['user_id' => Yii::$app->user->id])->andWhere(['end_datetime' => null])->one()->id;

        Yii::info('ID не завершенной смены: ' . $changeID, __METHOD__);

        //Получаем агента, зарегистрировавшего водителя.
        $creator = $model->creator_id;
        //Получаем текущую (не завершенную) смену агента, который зарегистрировал водителя
        $changeAgent = Change::find()->where(['user_id' => $creator])->andWhere(['end_datetime' => null])->one()->id;

        Yii::info('ID смены агента, зарегистрировавшего водителя: ' . $changeAgent, __METHOD__);


        $arr_act[] = $changeID;
        $arr_act[] = $changeAgent;

        Change::setActivation($arr_act);

        $model->save();

        //Отправляем сообщение в телеграм-канал
        ClientController::actionPushtelegram('Ура! Активатор ' . Users::getShortName($model->activator_id) . ' только что активировал водителя!', true);

        Yii::$app->session->setFlash('success', 'Водитель ' . User::getShortName($model->id) . ' успешно активирован!');

        return $this->redirect(['index']);
    }

    /**
     * Отклонение активации
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionRejectActivation()
    {
        $request = Yii::$app->request;

        if ($request->isPost) {
            $id = trim($request->post('id'));
            $comment = $request->post('comment');
        } else {
            $id = trim($request->get('id'));
            $comment = $request->get('comment');
        }


        Yii::info('ID: ' . $id, 'test');
        Yii::info('Comment: ' . $comment, 'test');

        $model = $this->findModel($id);
        if ($model) {
            $model->comment = $comment;
            $model->status_id = Drivers::DRIVER_STATUS_REJECTED;

            Yii::info('status_id: ' . $model->status_id, 'test');

            $model->save();

            return $this->redirect(['index']);

//            Yii::$app->response->format = Response::FORMAT_JSON;
//
//            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }
        return 'Ошибка';

    }

    /**
     * Отправка на переактивацию
     *
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionReactivate($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model->load(Yii::$app->request->post());
        $model->status_id = 1;
        $model->activator_id = null;
        $model->save();
        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true,
        ];
//        $this->redirect('index');
    }

    /**
     * Подтверждение регистрации
     *
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionConfirm($id)
    {
        Yii::info('ID: ' . $id, __METHOD__);

        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status_id = 1;
        $model->save();
        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true,
        ];
//        $this->redirect('index');
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCorrect()
    {
        $request = Yii::$app->request;

        $id = trim($request->get('id'));
        $comment = $request->get('comment');

        Yii::info('ID: ' . $id, __METHOD__);
        Yii::info('Comment: ' . $comment, __METHOD__);

        $model = $this->findModel($id);
        $model->comment = $comment;
        $model->status_id = Drivers::DRIVER_STATUS_ON_CORRECTION;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Получает водителей из Ситимобил
     * @throws \yii\base\Exception
     */
    public function actionGetDrivers()
    {
//        $params['id_driver'] = ['2674xc082737333c521d547afd35e2c486dfa'];
        //Получение списка водителей
//        $result = Car::requestAPI('getCarOptions', [], 'GET');
//        $result = Car::requestAPI('getDriverData', ['id_driver' => 52]);
//        $result = Car::requestAPI('getCarOptions', [
//            'marks' => [
//                'id' => 7,
//                'name' => 'Audi'
//            ]
//
//        ], 'GET');
        $result = ClientController::requestAPI('getDriversList', [], 'GET');
        VarDumper::dump($result, 10, true);
    }

    /**
     * @param $id
     */
    public function actionDownloadPhotos($id)
    {

        $path_folder = 'uploads/' . $id;
        $zip = new ZipArchive(); // подгружаем библиотеку zip

        $tmp_dir = 'uploads/tmp/';
        if (!is_dir($tmp_dir)) {
            mkdir($tmp_dir);
        }

        $zip_name = $tmp_dir . time() . ".zip"; // имя файла

        if ($zip->open($zip_name, ZIPARCHIVE::CREATE) !== TRUE) {
            Yii::error('Ошибка создания архива', __METHOD__);
            throw new  Exception("Ошибка создания архива");
        }
        Yii::info('имя архива ' . $zip_name, __METHOD__);

        $scanned_directory = FileHelper::findFiles($path_folder);


        Yii::info($scanned_directory, __METHOD__);

        foreach ($scanned_directory as $file) {

            Yii::info(basename($file), __METHOD__);

            $zip->addFile($file, basename($file)); // добавляем файлы в zip архив
        }
        $zip->close();

        if (ob_get_level()) {
            ob_end_clean();
        }

        Yii::$app->response->sendFile(Yii::getAlias($zip_name))->on(Response::EVENT_AFTER_SEND, function ($event) {
            unlink($event->data);
        }, $zip_name);
    }

    /**
     * Отображает форму загрузки фото
     *
     * @param string $type Тип загружамых фото (profile|documents|car)
     * @return array
     */
    public function actionShowDropzone($type)
    {
        $type_text = '';

        switch ($type) {
            case 'profile':
                $type_text = 'Водитель';
                break;
            case 'documents':
                $type_text = 'Документы';
                break;
            case 'car':
                $type_text = 'Автомобиль';
                break;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'title' => "{$type_text}. Загрузка фотографий",
            'content' => $this->renderAjax('_dropzone', ['type' => $type]),
            'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-right', 'data-dismiss' => "modal"])
        ];
    }

    // Удаление фотографий с сервера.
    public function actionRemoveFile($filename)
    {

        $request = Yii::$app->request;
        $temp_folder = $request->cookies->getValue('tmpFolder');

        $image_path = 'uploads/' . $temp_folder . '/' . $filename;
        Yii::info('Путь к удаляемой картинке: ' . $image_path);
        if (file_exists($image_path)) unlink(Yii::getAlias($image_path));
    }

    //Загрузка фото на сервер.
    public function actionUpload($type)
    {
        $session = Yii::$app->session;


        $temp_folder = $session->get('tmpFolder');

        Yii::info($temp_folder, __METHOD__);

        if (!$temp_folder) {

            $temp_folder = time();

            $session->set('tmpFolder', $temp_folder);

        }

        $uploadPath = 'uploads/' . $temp_folder . '/' . $type . '/';

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0755, true);
        }
        Yii::info($uploadPath, __METHOD__);
        Yii::info($_FILES, __METHOD__);

        if (isset($_FILES['image'])) {
            $file = UploadedFile::getInstanceByName('image');
            $original_name = $file->baseName;
            $newFileName = $original_name . '.' . $file->extension;
            // you can write save code here before uploading.
            if (!$file->saveAs($uploadPath . '/' . $newFileName)) {
                Yii::error('Ошибка сохранения изображения', __METHOD__);
            }
        } else {
            /*return $this->render('create', [
                'model' => new Products(),
            ]);*/
        }
    }


}
