<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Car */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registration_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'car_license')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'car_license_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'car_date_license')->textInput() ?>

    <?= $form->field($model, 'car_date_license_end')->textInput() ?>

    <?= $form->field($model, 'mark_id')->textInput() ?>

    <?= $form->field($model, 'model_id')->textInput() ?>

    <?= $form->field($model, 'year_manufacture')->textInput() ?>

    <?= $form->field($model, 'sts_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_sts_front_side')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_sts_reverse_side')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_license_front_side')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_license_reverse_side')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_dot_a')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_dot_b')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_dot_v')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_dot_g')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_salon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_id')->textInput() ?>

    <?= $form->field($model, 'id_api')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
