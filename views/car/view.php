<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Car */
?>
<div class="car-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'color',
            'registration_number',
            'status',
            'car_license',
            'car_license_number',
            'car_date_license',
            'car_date_license_end',
            'mark_id',
            'model_id',
            'year_manufacture',
            'sts_number',
            'foto_sts_front_side',
            'foto_sts_reverse_side',
            'foto_license_front_side',
            'foto_license_reverse_side',
            'foto_dot_a',
            'foto_dot_b',
            'foto_dot_v',
            'foto_dot_g',
            'foto_salon',
            'driver_id',
            'id_api',
        ],
    ]) ?>

</div>
