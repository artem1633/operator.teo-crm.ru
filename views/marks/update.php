<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Marks */
?>
<div class="marks-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
