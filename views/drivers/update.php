<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Drivers */
$this->title = 'Изменить';
?>
<div class="drivers-update">

    <?= $this->render('_form', [
        'model' => $model,
        'car_model' => $car_model,
    ]) ?>

</div>
