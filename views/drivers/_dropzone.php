<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kato\DropZone;
use yii\web\JsExpression;

$request = Yii::$app->request;

if ($request->get('type')){
    $type = $request->get('type');
} else {
    $type = null;
}

?>
<?php $form = ActiveForm::begin([
    'class' => 'dropzone',
]); ?>
<div class="col-md-12">
    <?= DropZone::widget([
        'options' => [
            'url'=> Url::to(['/drivers/upload', 'type' => $type]),
            'paramName'=>'image',
            'maxFilesize' => '5',
            'addRemoveLinks' => true,
            'dictRemoveFile' => 'Удалить',
            'dictDefaultMessage' => "Кликните для выбора или перетащите файлы",
            'acceptedFiles' => 'image/*',
            'init' => new JsExpression("function(file){ 
                            var resultJsonInput = $('#drivers-photos').val();
                            if(resultJsonInput.length > 0) {
                                var resultArray = JSON.parse(resultJsonInput);
                            } else {
                                var resultArray = Array();
                            }

                            var length = resultArray.length;
                            var path = $('#path').val();
                            //alert(path);
                            for (i = 0; i < length; i++) {
                                
                                var file_image = $('#url').val() + resultArray[i].path;
                                //alert(file_image);
                                var mockFile = { name: resultArray[i].name, size: resultArray[i].size, type: resultArray[i].type };
                                this.options.addedfile.call(this, mockFile);
                                this.options.thumbnail.call(this, mockFile, file_image);
                                mockFile.previewElement.classList.add('dz-success');
                                mockFile.previewElement.classList.add('dz-complete');
                            }
     
                     }"),
        ],
        'clientEvents' => [
            'complete' => "function(file, index){
                         var image = {
                             name: file.name,
                             size: file.size,
                             type: file.type,
                             path: ($('#path').val()+file.name),
                         }; 
                         /*console.log(image);*/
                         var resultJsonInput = $('#drivers-photos').val();
                         if(resultJsonInput.length > 0) {
                             var resultArray = JSON.parse(resultJsonInput);
                         } else {
                             var resultArray = Array();
                         }
                         resultArray.push(image);
                         var JsonResult = JSON.stringify(resultArray);
                         $('#users-photos').val(JsonResult);

                        }",
            'removedfile' => "function(){
                alert(1231313);
                $.get('/drivers/remove-file',{'filename':file.name})
            }"
//            'removedfile' => "function(file){
//                            $.get('/drivers/remove-file',
//                            {'id':$('#number').val(), 'filename':file.name},
//                                function(data){
//
//                                var resultJsonInput = $('#drivers-photos').val();
//                                if(resultJsonInput.length > 0) {
//                                    var resultArray = JSON.parse(resultJsonInput);
//                                } else {
//                                    var resultArray = Array();
//                                }
//
//                                var index = -1;
//                                var length = resultArray.length;
//                                for (i = 0; i < length; i++) {
//                                    if( resultArray[i].name == file.name && resultArray[i].size == file.size && resultArray[i].type == file.type) index = i;
//                                }
//                                resultArray.splice(index,1);
//                                var JsonResult = JSON.stringify(resultArray);
//                                $('#users-photos').val(JsonResult);
//
//                                $.get('/users/set-image',
//                                {'id':$('#number').val(), 'photos':JsonResult},
//                                    function(data){ }
//                                );
//
//                                }
//                            );
//                        }"
        ],
    ]);
    ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>
    <?php ActiveForm::end(); ?>
</div>