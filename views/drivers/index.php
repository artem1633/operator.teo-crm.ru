<?php

use app\models\Drivers;
use app\models\DriverStatus;
use app\models\Users;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtelierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Водители';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
$columns = __DIR__ . '/_columns.php';

?>
<div class="driver-index">
    <?php
    If (Users::isAgent()) {
        $toolbar = [
            ['content' =>
//                Html::a('Ожидает (' . Drivers::getActivatedDeiversCount() . ')', ['index', 'show_expected' => true],
//                    [/*'role' => 'modal-remote',*/
//                        'data-pjax' => 0, 'title' => 'Показать', 'class' => 'btn btn-info']) .
                Html::a('Создать', ['create'],
                    [/*'role'=>'modal-remote', */
                        'data-pjax' => 0, 'title' => 'Создать', 'class' => 'btn btn-primary']) .
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Обновить']) .
                '{toggleData}'
            ],
        ];
    } elseif (Users::isActivator()) {
        $columns = __DIR__ . '/_columns_requests.php';
        $toolbar = [
            ['content' =>
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Обновить']) .
                '{toggleData}'
            ],
        ];
    } elseif (Users::isAdmin()) {
        $toolbar = ['content' =>
        //            Html::a('Получение списка водителей от CityMobil', ['testapi'],
        //                [/*'role' => 'modal-remote',*/
        //                    'data-pjax' => 0, 'title' => 'Показать', 'class' => 'btn btn-info']) .
        //            Html::a('Ожидает (' . Drivers::getActivatedDeiversCount() . ')', ['index', 'show_expected' => true],
        //                [/*'role' => 'modal-remote',*/
        //                    'data-pjax' => 0, 'title' => 'Показать', 'class' => 'btn btn-info']) .
            Html::a('Создать', ['create'],
                [/*'role'=>'modal-remote', */
                    'data-pjax' => 0, 'title' => 'Создать', 'class' => 'btn btn-primary']) .
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Обновить']) .
            '{toggleData}'
        ];
    }
    ?>

    <?php if ($expectedProvider && Users::isAgent()) { ?>
        <div class="expected-drivers">
            <?= GridView::widget([
                'id' => 'crud-datatable-expected',
                'caption' => 'Прошедшие активацию',
                'resizableColumns' => true,
                'dataProvider' => $expectedProvider,
//                'filterModel' => $searchModel,
                'pjax' => true,
                'responsiveWrap' => false,
                'columns' => require(__DIR__ . '/_columns.php'),
                'toolbar' => [
                    ['content' =>
                        Html::a('Скрыть', ['index', 'show_expected' => false],
                            [/*'role' => 'modal-remote',*/
                                'data-pjax' => 0, 'title' => 'Показать', 'class' => 'btn btn-info']) .
                        Html::a('Создать', ['create'],
                            [/*'role'=>'modal-remote', */
                                'data-pjax' => 0, 'title' => 'Создать', 'class' => 'btn btn-primary']) .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Обновить']) .
                        '{toggleData}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Ожидающие водители',
                    'before' => '',
                    'after' => '<div class="clearfix"></div>',
                ]
            ]) ?>

        </div>
    <?php } ?>
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' => true,
            'responsiveWrap' => false,
            'rowOptions' => function ($model, $key, $index, $grid) {
                return ['style' => 'background-color: ' . DriverStatus::find()->where(['id' => $model->status_id])->one()->color . ';'];
            },
            'columns' => require($columns),
            'toolbar' => $toolbar,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i> Список',
                'before' => '',
                'after' => '<div class="clearfix"></div>',
            ]
        ]) ?>
    </div>


</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
