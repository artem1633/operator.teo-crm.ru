<?php

use app\models\Company;
use app\models\Drivers;
use app\models\DriverStatus;
use app\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;

$buttons = [];

if (Users::isAdmin()) {
//    if (YII_ENV_DEV){
    $template = '{confirm} {reactivate} {accept} {reject} {take_activate} {view} {update} {delete}';
//    } else {
//        $template = '{confirm} {accept} {reject} {take_activate} {view} {update} {delete}';
//    }
    $buttons = [
        'confirm' => function ($url, $model) {
            //Подтверждение неподтвержденного пользователя
            $url = Url::to(['confirm', 'id' => $model->id]);
            return Html::a('<span class="fa fa-thumbs-o-up"></span>', $url, [
                'class' => 'btn btn-warning btn-sm',
                'title' => 'Подтвердить регистрацию',
                'data-toggle' => 'tooltip',
                'role' => 'modal-remote',
            ]);
        },
        'reactivate' => function ($url, $model) {
            $url = Url::to(['reactivate', 'id' => $model->id]);
            return Html::a('<span class="fa fa-hand-pointer-o"></span>', $url, [
                'class' => 'btn btn-info btn-sm',
                'title' => 'Отправить на повторную активацию',
                'data-toggle' => 'tooltip',
                'role' => 'modal-remote',
            ]);
        },
        'accept' => function ($url, $model) {
            $url = Url::to(['accept-activation', 'id' => $model->id]);
            return Html::a('<span class="fa fa-handshake-o"></span>', $url, [
                'class' => 'btn btn-success btn-sm',
                'title' => 'Подтвердить водителя',
                'data-toggle' => 'tooltip'
            ]);
        },
        'reject' => function ($url, $model) {
            $url = Url::to(['reject-activation', 'id' => $model->id]);
            return Html::a('<span class="fa fa-hand-paper-o"></span>', $url, [
                'class' => 'btn btn-danger btn-sm',
                'role' => 'modal-remote',
                'title' => 'Отклонить водителя',
//                'data-toggle' => 'tooltip'
            ]);
        },
        'take_activate' => function ($url, $model) {
            $url = Url::to(['take-on-activation', 'id' => $model->id]);
            return Html::a('<span class="fa fa-exclamation"></span>', $url, [
                'class' => 'btn btn-info btn-sm',
                'title' => 'Взять на активацию',
                'data-toggle' => 'tooltip'
            ]);
        },
        'update' => function ($url, $model) {
            $url = Url::to(['update', 'id' => $model->id]);
            return Html::a('<span class="fa fa-pencil-square-o"></span>', $url, [
                'class' => 'btn btn-success btn-sm',
//                'role' => 'modal-remote',
                'title' => 'Изменить',
                'data-toggle' => 'tooltip'
            ]);
        },
        'view' => function ($url, $model) {
            $url = Url::to(['view', 'id' => $model->id]);
            return Html::a('<span class="fa fa-eye"></span>', $url, [
                'class' => 'btn btn-primary btn-sm',
//                'role' => 'modal-remote',
                'title' => 'Просмотр',
                'data-toggle' => 'tooltip'
            ]);
        },
        'delete' => function ($url, $model) {
            $url = Url::to(['delete', 'id' => $model->id]);
            return Html::a('<span class="fa fa-trash-o"></span>', $url, [
                'class' => 'btn btn-danger btn-sm',
                'role' => 'modal-remote', 'title' => 'Удалить',
                'data-confirm' => false, 'data-method' => false,// for overide yii data api
                'data-request-method' => 'post',
                'data-toggle' => 'tooltip',
                'data-confirm-title' => 'Подтвердите действие',
                'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
            ]);
        },
    ];
} elseif (Users::isAgent()) {
    $template = '{confirm} {view} {update}';
    $buttons = [
        'confirm' => function ($url, $model) {
            //Подтверждение неподтвержденного пользователя
            $url = Url::to(['update', 'id' => $model->id]);
            if ($model->status_id == 5) {
                return Html::a('<span class="fa fa-thumbs-o-up"></span>', $url, [
                    'class' => 'btn btn-warning btn-sm',
                    'title' => 'Подтвердить регистрацию',
                    'data-toggle' => 'tooltip',
//                    'role' => 'modal-remote',
                ]);
            }
        },
        'update' => function ($url, $model) {
            $url = Url::to(['update', 'id' => $model->id]);
            if ($model->status_id != Drivers::DRIVER_STATUS_TO_ACTIVATION && $model->status_id != Drivers::DRIVER_STATUS_REJECTED) { //Если у водителя статус != "На активации"
                return Html::a('<span class="fa fa-pencil-square-o"></span>', $url, [
                    'class' => 'btn btn-success btn-sm',
                    'title' => 'Изменить',
                    'data-toggle' => 'tooltip'
                ]);
            }
        },
        'view' => function ($url, $model) {
            $url = Url::to(['view', 'id' => $model->id]);
            return Html::a('<span class="fa fa-eye"></span>', $url, [
                'class' => 'btn btn-primary btn-sm',
//                'role' => 'modal-remote',
                'title' => 'Просмотр',
                'data-toggle' => 'tooltip'
            ]);
        },

    ];
} elseif (Users::isActivator()) {
    $template = '{accept} {reject} {take_activate} {view}';
    $buttons = [
        //Активация
        'accept' => function ($url, $model) {
            $url = Url::to(['accept-activation', 'id' => $model->id]);
            if ($model->status_id == Drivers::DRIVER_STATUS_TO_ACTIVATION && $model->activator_id == Yii::$app->user->id) { // статус == "На активации", activator_id == текущий пользователь
                return Html::a('<span class="fa fa-handshake-o"></span>', $url, [
                    'class' => 'btn btn-success btn-sm',
                    'title' => 'Подтвердить водителя',
                    'data-toggle' => 'tooltip'
                ]);
            }
        },
        //Отказ в активации
        'reject' => function ($url, $model) {
            $url = Url::to(['reject-activation', 'id' => $model->id]);
            if (Users::isActivator() && $model->status_id == Drivers::DRIVER_STATUS_TO_ACTIVATION && $model->activator_id == Yii::$app->user->id)
                return Html::a('<span class="fa fa-hand-paper-o"></span>', $url, [
                    'class' => 'btn btn-danger btn-sm',
                    'role' => 'modal-remote',
                    'title' => 'Отклонить водителя',
                    'data-toggle' => 'tooltip'
                ]);
        },
        //Взять на активацию
        'take_activate' => function ($url, $model) {
            $url = Url::to(['take-on-activation', 'id' => $model->id]);
            if ($model->status_id != Drivers::DRIVER_STATUS_TO_ACTIVATION || $model->status_id != Drivers::DRIVER_STATUS_ACTIVATED) { //Если статус != "На активации" или статус != "Активирован"
                return Html::a('<span class="fa fa-exclamation"></span>', $url, [
                    'class' => 'btn btn-info btn-sm',
                    'title' => 'Взять на активацию',
                    'data-toggle' => 'tooltip'
                ]);
            }
        },
        //ПРосмотр
        'view' => function ($url, $model) {
            $url = Url::to(['view', 'id' => $model->id]);
            return Html::a('<span class="fa fa-eye"></span>', $url, [
                'class' => 'btn btn-primary btn-sm',
//                'role' => 'modal-remote',
                'title' => 'Просмотр',
                'data-toggle' => 'tooltip'
            ]);
        },
    ];
} else {
    $template = '{view}';
    $buttons = [
        'view' => function ($url, $model) {
            $url = Url::to(['view', 'id' => $model->id]);
            return Html::a('<span class="fa fa-eye"></span>', $url, [
                'class' => 'btn btn-primary btn-sm',
//                'role' => 'modal-remote',
                'title' => 'Просмотр',
                'data-toggle' => 'tooltip'
            ]);
        },
    ];
}

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'company_id',
        'label' => 'Компания',
        'value' => function ($data) {
            return Company::getName($data->company_id);
        },
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'foto_driver',
        'content' => function ($data) {

            return Drivers::getPhoto($data->id);
//            return Html::img(
//                $data->foto_driver ? "@web/uploads/" . $data->id . '/' . $data->foto_driver : "@web/images/nouser.png",
//                ['alt' => 'Фото водителя', 'class' => 'direct-chat-img']);
        },
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'surname',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'middle_name',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'phone',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'birthday',
        'format' => 'date',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status_id',
        'value' => function ($data) {
            $satus = DriverStatus::find()->where(['id' => $data->status_id])->one()->name;
            return $satus;
        },
        'vAlign' => 'middle',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'password_number',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'issued_by',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'issue_date_passport',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'number_bu',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'class_bu',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'issue_date_bu',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'callsign',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_passport',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_propiska',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_bu_front_side',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_bu_reverse_side',
    // ],


    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'creator_id',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'activator_id',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'date_create',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'date_activation',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'status_api',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'comment',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'mark_id',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'model_id',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'year_manufacture',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'state_number',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'sts_number',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'resolution',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'license',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_sts_front_side',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_sts_reverse_side',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_license_front_side',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_license_reverse_side',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_dot_a',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_dot_b',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_dot_v',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_dot_g',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foto_salon',
    // ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'template' => '{update} {delete}',
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//                return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
//        'updateOptions'=>['data-pjax'=>'0','title'=>'Изменить', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
//                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                          'data-request-method'=>'post',
//                          'data-toggle'=>'tooltip',
//                          'data-confirm-title'=>'Подтвердите действие',
//                          'data-confirm-message'=>'Вы уверены что хотите удалить этих элементов?'],
//    ],


    [
        'class' => 'yii\grid\ActionColumn',
        'headerOptions' => ['width' => '160'],
        'template' => $template,
        'buttons' => $buttons,
    ]

];   