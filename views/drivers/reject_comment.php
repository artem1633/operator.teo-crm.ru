<?php

use yii\bootstrap\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Drivers */
?>
<div class="drivers-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'surname',
            'name',
            'middle_name',
            'phone',
        ],
    ]) ?>
    <h3>Оставьте комментарий</h3>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'comment')->textarea() ?>

    <?php if (!Yii::$app->request->isAjax) {
        echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);
    }
    ?>

    <?php ActiveForm::end(); ?>

</div>
