<?php

use app\models\Drivers;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'surname',
        'label' => 'ФИО',
        'format' => 'raw',
        'value' => function ($data) {
            $fio = $data->surname . ' ' . $data->name . ' ' . $data->middle_name;
            return Html::a($fio, 'https://city-mobil.ru/taxiserv/driver/2674x' . $data->id, [
                'style' => 'color: black;',
                'target' => '_blank',
            ]);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'phone',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'UID',
        'attribute' => 'id',
        'value' => function ($data) {
            return $data->driver_company_outer_id;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'comment',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status_id',
        'format' => 'raw',
        'label' => 'Статус',
        'value' => function ($data) {
            if ($data->status_id == Drivers::DRIVER_STATUS_FREE) {
                return Html::a('Взять тикет', ['take-on-activation', 'id' => $data->id], [
                    'class' => 'btn btn-primary btn-sm',
                    'style' => 'color: white;',
                ]);
            } elseif ($data->status_id == Drivers::DRIVER_STATUS_REJECTED) {
                return Html::tag('div', 'Отклонен', ['style' => 'color: white;']);
            } elseif ($data->status_id == Drivers::DRIVER_STATUS_ACTIVATED) {
                return Html::tag('div', 'Активирован', ['style' => 'color: white;']);
            } elseif ($data->status_id == Drivers::DRIVER_STATUS_ON_CORRECTION) {
                return Html::tag('div', 'На исправлении', ['style' => 'color: white;']);

            } elseif ($data->status_id == Drivers::DRIVER_STATUS_TO_ACTIVATION) {
                if (Drivers::find()->where(['id' => $data->id])->one()->activator_id == Yii::$app->user->id) {
                    $url = Url::to(['update', 'id' => $data->id]);

                    return Html::a('<span class="fa fa-pencil"> На активации</span>', $url, [
                        'class' => 'btn btn-primary btn-sm',
//                        'role' => 'modal-remote',
                        'title' => 'На активации',
                        'data-toggle' => 'tooltip'
                    ]);

                } else {
                    return Html::tag('div', 'На активации');
                }


            }
        }

    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'headerOptions' => ['width' => '160'],
        'template' => '{view} {reactivate}',
        'buttons' => [
            'view' => function ($url, $model) {
                $url = Url::to(['view', 'id' => $model->id]);
                return Html::a('<span class="fa fa-eye"></span>', $url, [
                    'class' => 'btn btn-primary btn-sm',
//                'role' => 'modal-remote',
                    'title' => 'Просмотр',
                    'data-toggle' => 'tooltip'
                ]);
            },
//            Отмена активации
            'reactivate' => function ($url, $model) {
                $url = Url::to(['reactivate', 'id' => $model->id]);
                return Html::a('<span class="fa fa-close"></span>', $url, [
                    'class' => 'btn btn-info btn-sm',
                    'title' => 'Отменить активацию',
                    'data-toggle' => 'tooltip',
                    'role' => 'modal-remote',
                ]);
            },
        ],
    ]

];