<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Drivers */
$this->title = $model->isNewRecord ? 'Создание' : 'Редактирование' ;

?>
<div class="drivers-create">
    <?= $this->render('_form', [
        'model' => $model,
        'car_model' => $car_model,
    ]) ?>
</div>
