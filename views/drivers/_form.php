<?php

use app\models\Car;
use app\models\Company;
use app\models\Drivers;
use app\models\Settings;
use app\models\User;
use app\models\Users;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use yii\widgets\MaskedInput;
use newerton\fancybox\FancyBox;

/* @var $this yii\web\View */
/* @var $model app\models\Drivers */
/* @var $car_model app\models\Car */
/* @var $form yii\widgets\ActiveForm */

CrudAsset::register($this);

//\yii\helpers\VarDumper::dump($car_model, 10, true);
$carOptions = Car::getCarOptions();

$colors = ArrayHelper::map($carOptions['colors'], 'color', 'color');
$colors = Drivers::arrPushPos('Жёлтый', 'Жёлтый', 1, $colors); //Копируем элемент в начало массива
$colors = Drivers::arrPushPos('Белый', 'Белый', 1, $colors);

$marks = ArrayHelper::map($carOptions['marks'], 'name', 'name');

//VarDumper::dump($marks, 10, true);
Users::isActivator() ? $disabled = true : $disabled = false;

echo FancyBox::widget([
    'target' => 'a[rel=fancybox]',
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
        'width' => '10%',
        'height' => '10%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => false,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
    ]
]);


?>

    <div class="panel panel-primary">
        <?php
        $sms_gate_login = Settings::getSettingValueByKey('sms_gate_login');
        $sms_gate_password = Settings::getSettingValueByKey('sms_gate_password');
        $api_key = Settings::getSettingValueByKey('api_key');
        $sms_text = Settings::getSettingValueByKey('sms_text');

        if (!$sms_gate_login || !$sms_gate_password) {
            ?>
            <div class="alert alert-warning">Отсутствует логин или папроль СМС шлюза. Проверьте настройки</div>
        <?php } elseif (!$api_key) { ?>
            <div class="alert alert-warning">Отсутствует ключ доступа ("АПИ ключ") к сервису CityMobil. Проверьте
                настройки
            </div>
        <?php } elseif (!$sms_text) { ?>
            <div class="alert alert-warning">Отсутствует текст СМС сообщения отправляемого с кодом подтверждения.
                Проверьте
                настройки
            </div>
        <?php } ?>

        <div class="panel-heading">
            <h3 class="panel-title">
                Водитель <?php If (!$model->isNewRecord) {
                    echo Drivers::getShortName($model->id);
                } ?></h3>
        </div>
        <div id="id-driver" class="hidden">
            <?= $model->id ?>
        </div>
        <div class="container-fluid">
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'fieldConfig' => [
                        'enableLabel' => false,
                    ]
                ]); ?>
                <div class="driver-panel">
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'surname')->textInput(['maxlength' => true, 'disabled' => $disabled, 'placeholder' => 'Фамилия']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => $disabled, 'placeholder' => 'Имя']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true, 'disabled' => $disabled, 'placeholder' => 'Отчество']) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'phone')->begin() ?>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone fa-lg"
                                                                   aria-hidden="true"></i></span>
                                <?php if (User::isAgent() || !$disabled) { ?>
                                    <?= $form->field($model, 'phone', ['template' => '{input}'])->widget(MaskedInput::className(), [
                                        'mask' => '7 999 999 99 99',
                                    ]);
                                    ?>
                                <?php } else { ?>
                                    <?= $form->field($model, 'phone', ['template' => "{label}\n{input}"])->textInput(['disabled' => $disabled]); ?>
                                <?php } ?>
                            </div>
                            <?= $form->field($model, 'phone')->end() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <?php Yii::info('birthday: ' . $model->birthday, 'test') ?>
                            <?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
                                'options' => [
                                    'placeholder' => 'Дата рождения',
                                    'value' => $model->birthday ? Yii::$app->formatter->asDate($model->birthday) : null,
                                ],
                                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                                'removeButton' => false,
                                'disabled' => $disabled,
                                'pluginOptions' => [
                                    'startView' => 'decade',
                                    'autoclose' => true,
                                    'language' => 'ru',
                                    'format' => 'dd.mm.yyyy',
                                ]
                            ]);
                            ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'number_bu')->textInput(['maxlength' => true, 'disabled' => $disabled, 'placeholder' => 'Номер ВУ']) ?>
                        </div>
                        <div class="col-md-3">
                            <?php Yii::info('Дата выдачи ВУ: ' . $model->issue_date_bu, 'test') ?>

                            <?= $form->field($model, 'issue_date_bu')->widget(DatePicker::classname(), [
                                'options' => [
                                    'placeholder' => 'Дата выдачи ВУ',
                                    'value' => $model->issue_date_bu ? Yii::$app->formatter->asDate($model->issue_date_bu) : null,
                                ],
                                'removeButton' => false,
                                'disabled' => $disabled,
                                'pluginOptions' => [
                                    'startView' => 'decade',
                                    'autoclose' => true,
                                    'format' => 'dd.mm.yyyy',
                                ]
                            ]);
                            ?>
                        </div>
                        <div class="col-md-3">
                            <?php // echo $form->field($model, 'sts_number')->textInput(['maxlength' => true, 'disabled' => $disabled, 'placeholder' => 'Номер СТС']) ?>
                            <?= $form->field($model, 'passport_number')->begin() ?>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-address-card fa-lg"
                                                                   aria-hidden="true"></i></span>
                                <?php if (User::isAgent() || !$disabled) { ?>
                                    <?= $form->field($model, 'passport_number', ['template' => '{input}'])->textInput();
                                    ?>
                                <?php } else { ?>
                                    <?= $form->field($model, 'passport_number', ['template' => "{label}\n{input}"])->textInput(['disabled' => $disabled]); ?>
                                <?php } ?>
                            </div>
                            <?= $form->field($model, 'passport_number')->end() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'company_id')->widget(Select2::className(), [
                                'data' => Company::getList(),
                                'options' => [
                                    'placeholder' => 'Выберите компанию',
                                    'multiply' => false,
                                    'disabled' => $disabled
                                ]
                            ]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'need_sticker')->checkbox(['disabled' => $disabled])->label(true) ?>
                        </div>
                        <?php if (Users::isActivator() || Users::isAdmin()) { ?>
                            <div class="col-md-3 pull-right">
                                <?= $form->field($model, 'creator_id')->textInput([
                                    'maxlength' => true,
                                    'disabled' => true,
                                    'placeholder' => 'Агент',
                                    'value' => Users::getFullName($model->creator_id),
                                ]) ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php If (!$model->isNewRecord) { ?>
                                <?= $form->field($model, 'comment')->textarea([
                                    'id' => 'comment',
                                    'disabled' => Users::isAgent(),
                                    'placeholder' => 'Комментарий'
                                ]) ?>
                            <?php } ?>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <?= $form->field($model, 'percenttocharge')->hiddenInput([
                                    'value' => Settings::getSettingValueByKey('percent_to_charge'),
                                ])->label(false) ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'creditlimit')->hiddenInput([
                                    'value' => Settings::getSettingValueByKey('credit_limit'),
                                ])->label(false) ?>
                                <?= $form->field($model, 'driver_status')->hiddenInput(['maxlength' => true, 'disabled' => true])->label(false) ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'invited_by')->hiddenInput()->label(false) ?>
                            </div>
                        </div>

                    </div>

                </div>

                <!--        Информация про автомобиль-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <p>Информация об автомобиле</p>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($car_model, 'mark')->widget(Select2::classname(), [
                                    'data' => $marks,
                                    'options' => [
                                        'placeholder' => 'Выберите марку',
                                        'multiple' => false,
                                        'disabled' => $disabled,
                                        'onchange' => '$.post(
                                        "' . Url::toRoute('/car/model-lists') . '", 
                                        {mark: $(this).val()}, 
                                        function(res){
                                            $("#model").removeAttr("disabled");
                                            $("#model").html(res);
                                        }
                                        );'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]); ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($car_model, 'model')->widget(Select2::classname(), [
                                    'data' => $model->isNewRecord ? '' : [$car_model->model => $car_model->model],
                                    'options' => [
                                        'id' => 'model',
                                        'placeholder' => 'Выберите модель',
                                        'multiple' => false,
                                        'disabled' => $model->isNewRecord ? true : $disabled,
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]); ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($car_model, 'color')->widget(Select2::classname(), [
                                    'data' => $colors,
                                    'options' => [
                                        'placeholder' => 'Выберите цвет',
                                        'multiple' => false,
                                        'disabled' => $disabled,
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]); ?>

                            </div>
                            <div class="col-md-4">
                                <?= $form->field($car_model, 'registration_number')->textInput(['maxlength' => true, 'disabled' => $disabled, 'placeholder' => 'Гос. номер']) ?>

                            </div>
                            <div class="col-md-4">
                                <?= $form->field($car_model, 'year_manufacture')->textInput(['disabled' => $disabled, 'placeholder' => 'Год выпуска']) ?>
                            </div>

                            <div class="col-md-6">
                                <?= $form->field($car_model, 'car_license')->textInput(['disabled' => $disabled, 'placeholder' => 'Номер разрешения']) ?>

                            </div>
                            <div class="col-md-6">
                                <?= $form->field($car_model, 'car_license_number')->textInput(['disabled' => $disabled, 'placeholder' => 'Номер бланка разрешения']) ?>

                            </div>
                            <div class="col-md-6">
                                <?= $form->field($car_model, 'car_date_license')->widget(DatePicker::classname(), [
                                    'options' => [
                                        'placeholder' => 'Дата выдачи разрешения',
                                        'value' => $car_model->car_date_license ? Yii::$app->formatter->asDate($car_model->car_date_license) : null,
                                        'disabled' => $disabled,
                                    ],
                                    'removeButton' => false,
                                    'pluginOptions' => [
                                        'startView' => 'decade',
                                        'autoclose' => true,
                                        'format' => 'dd.mm.yyyy',
                                    ]
                                ]);
                                ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($car_model, 'car_date_license_end')->widget(DatePicker::classname(), [
                                    'options' => [
                                        'placeholder' => 'Дата окончания разрешения',
                                        'value' => $car_model->car_date_license_end ? Yii::$app->formatter->asDate($car_model->car_date_license_end) : null,
                                        'disabled' => $disabled
                                    ],
                                    'removeButton' => false,
                                    'pluginOptions' => [
                                        'startView' => 'decade',
                                        'autoclose' => true,
                                        'format' => 'dd.mm.yyyy',
                                    ]
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--                Файлы-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p>Файлы</p>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php if (!$disabled) { ?>
                            <div class="col-md-3">
                                <?= Html::a('Загрузить фото водителя', ['show-dropzone', 'type' => 'profile'], [
                                    'role' => 'modal-remote',
                                    'data-pjax' => 1,
                                    'title' => 'Создать',
                                    'class' => 'btn btn-primary',
                                ]) ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::a('Загрузить документы', ['show-dropzone', 'type' => 'documents'], [
                                    'role' => 'modal-remote',
                                    'data-pjax' => 1,
                                    'title' => 'Создать',
                                    'class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= Html::a('Загрузить фото автомобиля', ['show-dropzone', 'type' => 'car'], [
                                    'role' => 'modal-remote',
                                    'data-pjax' => 1,
                                    'title' => 'Создать',
                                    'class' => 'btn btn-primary']) ?>
                            </div>
                        <?php } else { ?>
                            <div class="col-md-4">
                                <?= Html::a('Скачать все фотографии', Url::to(['download-photos', 'id' => $model->id]), ['class' => 'btn btn-primary']) ?>
                            </div>

                        <?php } ?>

                    </div>
                </div>
            </div>
        </div><!-- end panel-body-->
    </div>

<?php if (!Users::isActivator()) { ?>
    <div class="row">
    <div class="col-md-12">
        <?php if ($model->isNewRecord || $model->status_id == Drivers::DRIVER_STATUS_NOT_CONFIRMED) { ?>
            <?= Html::a($model->isNewRecord ? 'Создать' : 'Сохранить', ['#'], [
                'class' => 'btn btn-primary',
                'data-toggle' => 'modal',
                'data-target' => '.confirm-modal',
                'onClick' => 'clear_confirm()',
            ]) ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success hidden', 'id' => 'submit_btn']) ?>
        <?php } else { ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="row">
        <div class="col-md-4">
            <small class="pull-left"><sup>*</sup>Для отправки на исправление или отклонения тикета
                необходимо ввести комментарий
            </small>
        </div>
        <div class="col-md-8">
            <div class="btn-group  pull-right">
                <?= Html::button('Завершить активацию', [
                    'class' => 'btn btn-success',
                    'data-href' => Url::toRoute(['accept-activation', 'id' => $model->id]),
                    'id' => 'accept-btn',
                ]) ?>
                <?= Html::button('Отклонить', [
                    'class' => 'btn btn-danger',
                    'data-href' => Url::toRoute(['reject-activation', 'id' => $model->id]),
                    'disabled' => true,
                    'id' => 'reject-btn',
                ]) ?>
                <?= Html::button('На исправление', [
                    'class' => 'btn btn-warning',
                    'data-href' => Url::toRoute(['correct', 'id' => $model->id]),
                    'disabled' => true,
                    'id' => 'correct-btn',
                ]) ?>
                <?= Html::a('Назад к списку активаций', ['index'], [
                    'class' => 'btn btn-info',
                    'id' => 'return-btn',
                ]) ?>
            </div>
        </div>
    </div>
<?php } ?>

    <div class="modal fade confirm-modal" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Необходимо подтверждение регистрации нового водителя</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="modal-head">
                            <div id="modal-head" class="col-md-12"></div>
                        </div>
                        <div class="col-md-12">
                            <button id="confirm-btn" onclick="send_sms()" type="button"
                                    class="btn btn-success btn-lg btn-block">Отправить СМС с кодом подтверждения
                            </button>
                            <div class="confirm-info alert"></div>
                        </div>

                        <div id="confirm-code" class="col-md-6 col-md-offset-3 hidden">
                            <div class="input-group">
                                <input id="input-code" type="text" class="form-control">
                                <span class="input-group-btn"><button onclick="checkCode()" class="btn btn-success"
                                                                      type="button">Подтвердить</button></span>
                            </div><!-- /input-group -->
                        </div>

                        <div class="col-md-12">
                            <div id="confirm-progress" class="progress progress-striped active hidden">
                                <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                                     style="width: 100%">
                                    <span class="sr-only">45% Complete</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary" onclick="saveNotConfirm()">Сохранить без
                        подтверждения
                    </button>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

<?php
$this->registerJsFile('/js/dform.js',
    ['depends' => [\yii\web\JqueryAsset::className()], \yii\web\View::POS_READY]);
?>