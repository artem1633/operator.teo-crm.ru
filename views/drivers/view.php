<?php

use app\models\Drivers;
use app\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Drivers */
/* @var $car_model app\models\Car */

$this->title = "Данные о водителе: " . Drivers::getShortName($model->id);

?>
<div class="drivers-view">

    <div class="header row">
        <div class="col-md-12">
            <?php
            echo Html::a('Вернуться', ['drivers/index'], ['class' => 'btn btn-info pull-right'])
            ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'surname',
                        'label' => 'ФИО',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return $data->surname . ' ' . $data->name . ' ' . $data->middle_name;
                        }
                    ],
                    'phone',
                    'callsign',
                    [
                        'attribute' => 'status_id',
                        'value' => function ($data) {
                            return \app\models\DriverStatus::getStatusNameById($data->status_id);
                        }
                    ],
                    [
                        'attribute' => 'creator_id',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Users::getFullName($data->creator_id);
                        },

                    ],
                    [
                        'attribute' => 'activator_id',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Users::getFullName($data->activator_id);
                        },

                    ],
                    'date_create:date',
                    'date_activation:date',
                    'status_api',
                    'comment:ntext',
//                    'license',
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Файлы</h3>
                    <?= Html::a('Скачать все фотографии', Url::to(['download-photos', 'id' => $model->id]), ['class' => 'btn btn-default'])?>
                </div>

            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <?= DetailView::widget([
                'model' => $car_model,
                'attributes' => [
                    'color',
                    'registration_number',
                    'status',
                    'car_license',
                    'car_license_number',
                    'car_date_license:date',
                    'car_date_license_end:date',
                    'mark',
                    'model',
                    'year_manufacture',
                ],
            ]) ?>
        </div>
    </div>
    <?php if ($model->status_id == Drivers::DRIVER_STATUS_TO_ACTIVATION && Users::isActivator()): ?>
    <div class="row">
        <div class="col-md-12 text-right">
            <?= Html::a('Завершить активацию', ['accept-activation', 'id' => $model->id, 'comment' => $model->comment], [
                'class' => 'btn btn-success',
            ]) ?>
        </div>
    </div>
    <?php endif ?>

</div>
