<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Change */
?>
<div class="change-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'begin_datetime',
            'end_datetime',
            'minut',
            'activation',
            'average_speed',
            'pay_sum',
            'status',
            'who_paid',
        ],
    ]) ?>

</div>
