<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Change */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="change-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'begin_datetime')->textInput() ?>

    <?= $form->field($model, 'end_datetime')->textInput() ?>

    <?= $form->field($model, 'minut')->textInput() ?>

    <?= $form->field($model, 'activation')->textInput() ?>

    <?= $form->field($model, 'average_speed')->textInput() ?>

    <?= $form->field($model, 'pay_sum')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'who_paid')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
