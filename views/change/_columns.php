<?php

use app\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;

if (Users::isAdmin()) {
    $template = '{view} {update} {delete} {close-duty}';

    $buttons = [
        'view' => function ($url, $model) {
            $url = Url::to(['view', 'id' => $model->id]);
            return Html::a('<span class="fa fa-eye"></span>', $url, [
                'class' => 'btn btn-primary btn-sm',
                'role' => 'modal-remote',
                'title' => 'Просмотр',
                'data-toggle' => 'tooltip'
            ]);
        },
        'update' => function ($url, $model) {
            $url = Url::to(['update', 'id' => $model->id]);
            return Html::a('<span class="fa fa-pencil-square-o"></span>', $url, [
                'class' => 'btn btn-success btn-sm',
                'role' => 'modal-remote',
                'title' => 'Изменить',
                'data-toggle' => 'tooltip'
            ]);
        },
        'close-duty' => function ($url, $model) {
            $url = Url::to(['close-duty', 'id' => $model->id]);
            return Html::a('<span class="fa fa-window-close-o"></span>', $url, [
                'class' => 'btn btn-danger btn-sm',
                'title' => 'Завершить смену',
            ]);
        },
        'delete' => function ($url, $model) {
            $url = Url::to(['delete', 'id' => $model->id]);
            return Html::a('<span class="fa fa-trash-o"></span>', $url, [
                'class' => 'btn btn-danger btn-sm',
                'role' => 'modal-remote', 'title' => 'Удалить',
                'data-confirm' => false, 'data-method' => false,// for overide yii data api
                'data-request-method' => 'post',
                'data-toggle' => 'tooltip',
                'data-confirm-title' => 'Подтвердите действие',
                'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
            ]);
        },
    ];
}


return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    ['class' => '\kartik\grid\DataColumn',
        'attribute' => 'user_id',
        'visible' => Users::isAdmin(),
        'label' => 'Пользователь',
        'value' => function ($data) {
            return Users::getShortName($data->user_id);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'visible' => Users::isAdmin(),

    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'begin_datetime',
        'format' => 'datetime',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'end_datetime',
        'format' => 'datetime',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'minut',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'activation',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'average_speed',
    ],
    [
        'class' => 'yii\grid\ActionColumn',
        'headerOptions' => ['width' => '160'],
        'visible' => Users::isAdmin(),
        'template' => $template,
        'buttons' => $buttons,
    ]


];   