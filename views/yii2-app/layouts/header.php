<?php

use app\models\Users;
use yii\helpers\Html;
use yii\bootstrap\Modal;

?>

    <header class="main-header">

        <?= Html::a('<span class="logo-mini">СГ</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

        <nav class="navbar navbar-static-top" role="navigation">

            <a href="#" onclick="$.post('/site/menu-position');" class="sidebar-toggle" data-toggle="push-menu"
               role="button"><span class="sr-only">Toggle navigation</span> </a>

            <?php if (!Users::isAdmin()) {
//                echo Html::a(
//                    'Завершить смену',
//                    ['/site/logout'],
//                    [
//                        'data-method' => 'post',
//                        'class' => 'btn btn-info btn-flat',
//                        'style' => 'margin: 1rem 5rem; padding: 0.5rem 5rem;'
//                    ]
//                );
            } ?>


            <div class="navbar-custom-menu">

                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"><?= Users::getShortName(Yii::$app->user->identity->id) ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="<?= 'http://' . $_SERVER['SERVER_NAME'] ?>/images/nouser.png"
                                     class="img-circle" alt="User Image"/>
                                <p> <?= Users::getShortName(Yii::$app->user->identity->id) ?> </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <?= Html::a('Профиль', ['users/update', 'id' => Yii::$app->user->identity->id],
                                        ['role' => 'modal-remote', 'title' => 'Профиль', 'class' => 'btn btn-default btn-flat']); ?>
                                </div>
                                <?php // echo Html::a('Изменить пароль', ['users/change', 'id' => Yii::$app->user->identity->id],['role' => 'modal-remote', 'title' => 'Изменить пароль', 'class' => 'btn btn-default btn-flat']); ?>
                                <div class="pull-right">
                                    <?= Html::a(
                                        'Завершить смену',
                                        ['/site/logout'],
                                        ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    ) ?>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => false,
    ],
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>