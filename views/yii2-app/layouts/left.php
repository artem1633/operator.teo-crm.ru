<?php

use app\models\Users;

$permission = Yii::$app->user->identity->permission;
$id = Yii::$app->user->identity->id;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?php

        if (Users::isAdmin()){
            $items = [
                ['label' => 'Menu', 'options' => ['class' => 'header']],
                ['label' => 'Компании', 'icon' => 'car', 'url' => ['/company'],],
                ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/users'],],
                ['label' => 'Таблица по сменам', 'icon' => 'file-text-o', 'url' => ['/change'],],
                ['label' => 'Водители', 'icon' => 'file-text-o', 'url' => ['/drivers'],],
                [
                    'label' => 'Справочники',
                    'icon' => 'book',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Статус водителя', 'icon' => 'cube', 'url' => ['/driver-status'],],
                        ['label' => 'Марка автомобиля', 'icon' => 'file-text-o', 'url' => ['/marks'],],
                        ['label' => 'Модель автомобиля', 'icon' => 'file-text-o', 'url' => ['/models'],],
                        ['label' => 'Настройки', 'icon' => 'file-text-o', 'url' => ['/settings'],],
                    ],
                ],
                ['label' => 'Профиль', 'icon' => 'file-text-o', 'url' => ['users/update', 'id' => Yii::$app->user->identity->id]],

            ];
        } elseif (Users::isActivator()){
            $items = [
                ['label' => 'Menu', 'options' => ['class' => 'header']],
                //['label' => 'Таблица по сменам', 'icon' => 'file-text-o', 'url' => ['/change'],],
                ['label' => 'Водители', 'icon' => 'file-text-o', 'url' => ['/drivers'],],
                ['label' => 'Профиль', 'icon' => 'file-text-o', 'url' => ['users/update', 'id' => Yii::$app->user->identity->id]],

            ];
        } else {
            $items = [
                ['label' => 'Menu', 'options' => ['class' => 'header']],
                ['label' => 'Водители', 'icon' => 'file-text-o', 'url' => ['/drivers'],],
                ['label' => 'Профиль', 'icon' => 'file-text-o', 'url' => ['users/update', 'id' => Yii::$app->user->identity->id]],
            ];
        }

        $items[] = ['label' => 'Завершить смену', 'icon' => 'sign-out', 'url' => ['/site/logout'],];


        if(isset(Yii::$app->user->identity->id))
            {
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => $items,
                    ]
                );
            }
        ?>

    </section>

</aside>
