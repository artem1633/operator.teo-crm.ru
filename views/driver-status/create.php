<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DriverStatus */

?>
<div class="driver-status-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
