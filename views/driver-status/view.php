<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DriverStatus */
?>
<div class="driver-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'color',
        ],
    ]) ?>

</div>
