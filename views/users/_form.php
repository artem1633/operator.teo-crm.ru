<?php

use app\models\Users;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php if (Users::isAdmin()) {?>

        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <?php if(Users::isAdmin() && $model->permission == 'agent'){ ?>
                <div class="col-md-12">
                    <?= $form->field($model, 'invited_by')->textInput(['maxlength' => true]) ?>
                </div>

            <?php } ?>

            <div class="col-md-8">
                <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'balance')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
                                    'mask' => '+7 (999) 999 99 99',
                                ])?>
                <?php // $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'telegram_nickname')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'login')->textInput([
                    'maxlength' => true,
                    'type' => 'text',
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'permission')->dropDownList($model->getRoleList(), [/*'prompt' => 'Выберите должность'*/]) ?>
            </div>
        </div>


        <?= $form->field($model, 'qiwi')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'payment_account')->textInput() ?>
        <?= $form->field($model, 'bik_bank')->textInput() ?>
        <?php // echo $form->field($model, 'recived_money')->dropDownList([Users::PAYMENT_OPTION_QIWI => 'QIWI', Users::PAYMENT_OPTION_CARD => 'Карта']) ?>

        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    <?php } else {?>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'surname')->textInput(['maxlength' => true, 'disabled' => true]) ?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => true]) ?>
            <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'disabled' => true]) ?>
            <?= $form->field($model, 'telegram_nickname')->textInput(['maxlength' => true]) ?>
            <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'qiwi')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'payment_account')->textInput() ?>
    <?= $form->field($model, 'bik_bank')->textInput() ?>
    <?php // echo $form->field($model, 'recived_money')->dropDownList([Users::PAYMENT_OPTION_QIWI => 'QIWI', Users::PAYMENT_OPTION_CARD => 'Карта']) ?>

    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    <?php } ?>
    
</div>
