<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'surname',
            'middle_name',
            'login',
            'password',
            'telegram_nickname',
            'permission',
            'balance',
            'party_system_id',
            'phone',
            'qiwi',
            'payment_account',
            'bik_bank',
            'recived_money',
        ],
    ]) ?>

</div>
