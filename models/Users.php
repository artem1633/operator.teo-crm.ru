<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $surname Фамилия
 * @property string $name Имя
 * @property string $middle_name Отчество
 * @property string $login Логин
 * @property string $password Пароль
 * @property string $permission Должность
 * @property double $balance Баланс
 * @property int $party_system_id Ид сторонней системы
 * @property string $phone Телефон
 * @property string $qiwi QIWI кошелек
 * @property int $payment_account Расчетный счет
 * @property int $bik_bank Бик банка
 * @property int $recived_money Способ вывода денег
 * @property string $invited_by ID пригласившего
 *
 * @property Change[] $changes
 */
class Users extends \yii\db\ActiveRecord
{
    const USER_ROLE_ADMIN = 'administrator';
    const USER_ROLE_AGENT = 'agent';
    const USER_ROLE_ACTIVATOR = 'activator';
    const PAYMENT_OPTION_QIWI = 1;
    const PAYMENT_OPTION_CARD = 2;

    public $new_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['balance'], 'number'],
            [['bik_bank', 'recived_money', 'payment_account', 'party_system_id'], 'integer'],
            [['login', 'invited_by', 'telegram_nickname', 'qiwi', 'phone', 'name', 'middle_name', 'surname', 'login', 'password', 'permission', 'new_password'], 'string', 'max' => 255],
            [['login'], 'unique'],
//            [['login'], 'email'],
            [['name', 'middle_name', 'surname', 'login'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'middle_name' => 'Отчество',
            'login' => 'Логин',
            'password' => 'Пароль',
            'permission' => 'Должность',
            'balance' => 'Баланс',
            'party_system_id' => 'Ид сторонней системы',
            'new_password' => 'Новый пароль',
            'phone' => 'Телефон',
            'qiwi' => 'QIWI',
            'payment_account' => 'Расчетный счет',
            'bik_bank' => 'Бик банка',
            'recived_money' => 'Способ получения денег',
            'telegram_nickname' => 'Ник в телеграме',
            'invited_by' => 'ID пригласившего',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->password = md5($this->password);
        }

        if ($this->new_password != null) {
            $this->password = md5($this->new_password);
        }
        return parent::beforeSave($insert);
    }

    public function getRoleList()
    {
        return ArrayHelper::map([
            ['id' => self::USER_ROLE_ADMIN, 'name' => 'Администратор',],
            ['id' => self::USER_ROLE_AGENT, 'name' => 'Агент',],
            ['id' => self::USER_ROLE_ACTIVATOR, 'name' => 'Активатор',],
        ], 'id', 'name');
    }

    public function getRoleDescription()
    {
        if (self::USER_ROLE_ADMIN == $this->permission) return 'Администратор';
        if (self::USER_ROLE_AGENT == $this->permission) return 'Агент';
        if (self::USER_ROLE_ACTIVATOR == $this->permission) return 'Активатор';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChanges()
    {
        return $this->hasMany(Change::className(), ['who_paid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Drivers::className(), ['activator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers0()
    {
        return $this->hasMany(Drivers::className(), ['creator_id' => 'id']);
    }

    /**
     * @return bool
     */
    public static function isAgent()
    {
        $permission = Users::find()->where(['id' => Yii::$app->user->id])->one()->permission;
        if ($permission == self::USER_ROLE_AGENT) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public static function isActivator()
    {
        $permission = Users::find()->where(['id' => Yii::$app->user->id])->one()->permission;
        if ($permission == self::USER_ROLE_ACTIVATOR) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public static function isAdmin()
    {
        $permission = Users::find()->where(['id' => Yii::$app->user->id])->one()->permission;
        if ($permission == self::USER_ROLE_ADMIN) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Возвращает фимилию и инициалы
     *
     * @param $id
     * @return string
     */
    public static function getShortName($id)
    {
        $model = Users::findOne($id);
        $n = mb_strtoupper(mb_substr($model->name, 0, 1)); //Получаем первую букву имени
        $mn = mb_strtoupper(mb_substr($model->middle_name, 0, 1)); //Первую букву отчества
        return $model->surname . ' ' . $n . '.' . $mn . '.' ;
    }

    public static function getFullName($id)
    {
        $model = Users::findOne($id);
        return $model->surname . ' ' . $model->name . ' ' . $model->middle_name;
    }

}
