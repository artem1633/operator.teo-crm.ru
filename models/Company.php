<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name Наименование компании
 * @property string $letter Буква для позывного водителя
 */
class Company extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['letter'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование компании',
            'letter' => 'Буква для позывного водителя',
        ];
    }

    /**
     * Получает имя компании по ID
     *
     * @param int $id Код компании
     * @return bool|string
     */
    public static function getName($id)
    {
        $model = self::findOne($id);

        if ($model) return $model->name;

        return false;
    }

    /**
     * Получает список компаний
     *
     * @return array|bool
     */
    public static function getList()
    {
        $models = self::find()->all();

        if ($models) return ArrayHelper::map($models, 'id', 'name');

        return false;
    }
}
