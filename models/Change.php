<?php

namespace app\models;

use app\modules\api\controllers\ClientController;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "change".
 *
 * @property int $id
 * @property string $begin_datetime Дата и время начала
 * @property string $end_datetime Дата и время завершения
 * @property int $minut Кол-во минут
 * @property int $activation Активаций
 * @property float $average_speed Средняя скорость
 * @property double $pay_sum Сумма оплаты
 * @property string $status Статус
 * @property int $who_paid Кто оплатил
 * @property int $user_id Код пользователя
 *
 * @property Users $whoPa
 */
class Change extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'change';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['begin_datetime', 'end_datetime'], 'safe'],
            [['minut', 'activation', 'who_paid', 'user_id'], 'integer'],
            [['average_speed', 'pay_sum'], 'number'],
            [['status'], 'string', 'max' => 20],
            [['who_paid'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['who_paid' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'begin_datetime' => 'Дата и время начала',
            'end_datetime' => 'Дата и время завершения ',
            'minut' => 'Кол-во минут',
            'activation' => 'Активаций ',
            'average_speed' => 'Средняя скорость',
            'pay_sum' => 'Сумма оплаты',
            'status' => 'Статус',
            'who_paid' => 'Кто оплатил',
            'user_id' => 'Код пользователя',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWhoPa()
    {
        return $this->hasOne(Users::className(), ['id' => 'who_paid']);
    }

    /**
     * Метод возвращает ИД незакрытой смены
     *
     * @return int|mixed
     */
    public static function getNotClosedShift()
    {
        $result = Change::find()->where(['user_id' => Yii::$app->user->id])->andWhere(['end_datetime' => null])->one()->id;
        Yii::warning($result);
        return $result;
    }

    /**
     * Метод закрытия смены
     *
     * @param $id
     * @throws NotFoundHttpException
     */
    public function CloseChange($id)
    {
        $model = Change::findOne($id);
        $model->end_datetime = date("Y-m-d H:i:s");
        $model->pay_sum = Settings::getSettingValueByKey('amount_for_one_activation') * $model->activation;
        $datetime1 = date_create($model->begin_datetime);
        $datetime2 = date_create($model->end_datetime);
        $interval = date_diff($datetime1, $datetime2);
        $model->minut = $interval->format('%i');;
        if ($model->minut) {
            $model->average_speed = round($model->activation / $model->minut, 2);
        }
        Yii::warning('Минут: ' . $model->minut, __METHOD__);
        Yii::warning('Скорость: ' . $model->average_speed, __METHOD__);
        $model->save();
    }

    /**
     * @param array $idChanges
     * @return void
     */
    public static function setActivation($idChanges)
    {
        foreach ($idChanges as $change) {
            $model = Change::findOne($change);
            if($model){
                $model->activation += 1;
                $model->save();
                break;
            }

        }
    }

    /**
     * @return string
     */
    public static function getReport()
    {
        $result = Users::find()//->select(['users.id', 'change.activation'])
        ->leftJoin('change', 'change.user_id = users.id')->where(['like', 'end_datetime', date('Y-m-d')])->limit(5)->all();

        Yii::warning($result, __METHOD__);

        $report = "";

        if ($result) {
            foreach ($result as $value) {
                $report .= Users::getShortName($value->id) . ' - ' . self::getActivationCount($value->id) . " активаций." . "\n";
            }
        } else {
            $report = 'На ' . date('d.m.Y') . ' отсутствуют данные для отчета';
        }

        Yii::warning($report, __METHOD__);

        return $report;
    }


    /**
     * Получает количество активаций смены, которая закрыта сегодня
     *
     * @param $user_id
     * @return mixed
     */
    private static function getActivationCount($user_id)
    {
        return Change::find()
            ->where(['user_id' => $user_id])
            ->andWhere(['BETWEEN', 'end_datetime', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')])
            ->one()->activation;
    }


}
