<?php

namespace app\models;

use app\modules\api\controllers\ClientController;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "drivers".
 *
 * @property int $id
 * @property string $surname Фамилия
 * @property string $name Имя
 * @property string $middle_name Отчество
 * @property string $phone Телефон
 * @property string $birthday Дата рождения
 * @property string $password_number Номер паспорта
 * @property string $issued_by Кем выдан (паспорт)
 * @property string $issue_date_passport Дата выдачи паспорта
 * @property string $number_bu Номер ВУ
 * @property string $class_bu Класс ВУ
 * @property string $issue_date_bu Дата выдачи ВУ
 * @property string $callsign Позывной
 * @property string $foto_passport Фото Паспорт главная страница
 * @property string $foto_propiska Фото Паспорт прописка
 * @property string $foto_bu_front_side Фото ВУ лицевая страница
 * @property string $foto_bu_reverse_side Фото ВУ оборотная страница
 * @property string $foto_driver Фото водителя
 * @property int $status_id Статус
 * @property int $creator_id Кто создал
 * @property int $activator_id Кто активировал
 * @property string $date_create Дата создания
 * @property string $date_activation Дата активации
 * @property int $status_api Статус Отправки по апи
 * @property string $comment Комментарий
 * @property int $mark_id Марка автомобиля
 * @property int $model_id Модель автомобиля
 * @property int $year_manufacture Год выпуска авто
 * @property string $state_number Гос. номер
 * @property string $sts_number Номер СТС
 * @property string $resolution Разрешение
 * @property string $license Серия и № лицензии
 * @property string $foto_sts_front_side Фото СТС лицевая сторона
 * @property string $foto_sts_reverse_side Фото СТС оборотная сторона
 * @property string $foto_license_front_side Фото Лицензии лицевая сторона
 * @property string $foto_license_reverse_side Фото Лицензии лицевая сторона
 * @property string $foto_dot_a Фото автомобиля с точка А
 * @property string $foto_dot_b Фото автомобиля с точка Б
 * @property string $foto_dot_v Фото автомобиля с точка В
 * @property string $foto_dot_g Фото автомобиля с точка Г
 * @property string $foto_salon Фото салона
 * @property float $percenttocharge Комиссия водителя
 * @property string $creditlimit Лимит баланса водителя
 * @property int $is_test Тестовый ли водитель
 * @property string $invited_by ID (внешнего сервиса) пригласившего водителя в компании
 * @property string $driver_status Статус водителя. A - допущен, B - заблокирован
 * @property boolean $need_sticker Нужена ли оклейка. True - нужна
 * @property string $data_sto Дата СТО
 * @property string $driver_company_outer_id ID сервиса CityMobil
 * @property string $company_id ID Компании
 * @property string $passport_number Серия и номер паспорта
 *
 * @property Users $activator
 * @property Users $creator
 * @property Marks $mark
 * @property Models $model
 * @property DriverStatus $status
 */
class Drivers extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file_foto_passport;
    public $file_foto_propiska;
    public $file_foto_bu_front_side;
    public $file_foto_bu_reverse_side;
    public $file_foto_driver;
    public $foto_a;
    public $foto_b;
    public $foto_v;
    public $foto_g;
    public $file_foto_salon;
    public $foto_license_front;
    public $foto_license_reverse;
    public $foto_sts_front;
    public $foto_sts_reverse;

    const DRIVER_STATUS_FREE = 1; //Свободен
    const DRIVER_STATUS_TO_ACTIVATION = 2; //На активации
    const DRIVER_STATUS_REJECTED = 3; //Отклонен
    const DRIVER_STATUS_ACTIVATED = 4; //Активирован
    const DRIVER_STATUS_NOT_CONFIRMED = 5; //Не подттвержден
    const DRIVER_STATUS_ON_CORRECTION = 6; //На исправлении

    public static function tableName()
    {
        return 'drivers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birthday', 'issue_date_passport', 'issue_date_bu', 'date_create', 'date_activation'], 'safe'],
            [['company_id', 'need_sticker', 'is_test', 'creditlimit', 'status_id', 'creator_id', 'activator_id', 'status_api', 'mark_id', 'model_id', 'year_manufacture'], 'integer'],
            [['driver_company_outer_id', 'comment'], 'string'],
            [['passport_number', 'data_sto', 'invited_by', 'driver_status', 'surname', 'name', 'middle_name', 'phone', 'password_number',
                'issued_by', 'number_bu', 'class_bu', 'callsign', 'foto_passport',
                'foto_propiska', 'foto_bu_front_side', 'foto_bu_reverse_side', 'foto_driver',
                'state_number', 'sts_number', 'resolution', 'license', 'foto_sts_front_side', 'foto_sts_reverse',
                'foto_sts_reverse_side', 'foto_license_front_side', 'foto_license_reverse_side',
                'foto_dot_a', 'foto_dot_b', 'foto_dot_v', 'foto_dot_g', 'foto_salon', 'foto_a',
                'foto_b', 'foto_v', 'foto_g', 'file_foto_bu_front_side', 'file_foto_bu_reverse_side', 'file_foto_driver'], 'string', 'max' => 255],
            [['activator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['activator_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['mark_id'], 'exist', 'skipOnError' => true, 'targetClass' => Marks::className(), 'targetAttribute' => ['mark_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => Models::className(), 'targetAttribute' => ['model_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => DriverStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['surname', 'name', 'middle_name', 'phone'], 'required'],
            [['percenttocharge'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'middle_name' => 'Отчество',
            'phone' => 'Телефон',
            'birthday' => 'Дата рождения',
            'password_number' => 'Номер паспорта',
            'issued_by' => 'Кем выдан (паспорт)',
            'issue_date_passport' => 'Дата выдачи паспорта',
            'number_bu' => 'Номер ВУ',
            'class_bu' => 'Класс ВУ',
            'issue_date_bu' => 'Дата выдачи ВУ',
            'callsign' => 'Позывной',
            'foto_passport' => 'Фото Паспорт главная страница',
            'foto_propiska' => 'Фото Паспорт прописка',
            'foto_bu_front_side' => 'Фото ВУ лицевая страница',
            'foto_bu_reverse_side' => 'Фото ВУ оборотная страница',
            'foto_driver' => 'Фото водителя',
            'status_id' => 'Статус',
            'creator_id' => 'Кто создал',
            'activator_id' => 'Кто активировал',
            'date_create' => 'Дата создания',
            'date_activation' => 'Дата активации',
            'status_api' => 'Статус Отправки по апи',
            'comment' => 'Комментарий ',
            'mark_id' => 'Марка автомобиля',
            'model_id' => 'Модель автомобиля',
            'year_manufacture' => 'Год выпуска авто',
            'state_number' => 'Гос. номер',
            'sts_number' => 'Номер СТС',
            'resolution' => 'Разрешение',
            'license' => 'Серия и № лицензии',
            'foto_sts_front_side' => 'Фото СТС лицевая сторона',
            'foto_sts_reverse_side' => 'Фото СТС оборотная сторона',
            'foto_license_front_side' => 'Фото Лицензии лицевая сторона',
            'foto_license_reverse_side' => 'Фото Лицензии оборотная сторона',
            'foto_dot_a' => 'Фото автомобиля с точка А',
            'foto_dot_b' => 'Фото автомобиля с точка Б',
            'foto_dot_v' => 'Фото автомобиля с точка В',
            'foto_dot_g' => 'Фото автомобиля с точка Г',
            'foto_salon' => 'Фото салона',

            'file_foto_passport' => 'Фото Паспорт главная страница',
            'file_foto_propiska' => 'Фото Паспорт прописка',
            'file_foto_bu_front_side' => 'Фото ВУ лицевая страница',
            'file_foto_bu_reverse_side' => 'Фото ВУ оборотная страница',
            'file_foto_driver' => 'Фото водителя',
            'foto_a' => 'Фото автомобиля с точка А',
            'foto_b' => 'Фото автомобиля с точка Б',
            'foto_v' => 'Фото автомобиля с точка В',
            'foto_g' => 'Фото автомобиля с точка Г',
            'file_foto_salon' => 'Фото салона',
            'foto_license_front' => 'Фото Лицензии лицевая сторона',
            'foto_license_reverse' => 'Фото Лицензии оборотная сторона',
            'foto_sts_front' => 'Фото СТС лицевая сторона',
            'foto_sts_reverse' => 'Фото СТС оборотная сторона',

            'percenttocharge' => 'Комиссия водителя',
            'creditlimit' => 'Лимит баланса водителя',
            'is_test' => 'Тестовый водитель',
            'invited_by' => 'ID пригласившего водителя в компании',
            'driver_status' => 'Статус водителя. (A - допущен, B - заблокирован)',
            'need_sticker' => 'Нужна оклейка',
            'data_sto' => 'Дата СТО',
            'driver_company_outer_id' => 'Код водитиля CityMobil',
            'company_id' => 'Код Компании',
            'passport_number' => 'Серия и номер паспорта',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->creator_id = Yii::$app->user->identity->id;
            $this->date_create = date('Y-m-d');
        }

        $driver = Yii::$app->request->post('Drivers');

        if (isset($driver)) {
            $birthday = str_replace('.', '-', $driver['birthday']);
            $issue_date_bu = str_replace('.', '-', $driver['issue_date_bu']);

            Yii::info('Водитель', 'test');
            Yii::info($driver, 'test');

            $this->birthday = date('Y-m-d', strtotime($birthday));
            $this->issue_date_bu = date('Y-m-d', strtotime($issue_date_bu));
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivator()
    {
        return $this->hasOne(Users::className(), ['id' => 'activator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Users::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMark()
    {
        return $this->hasOne(Marks::className(), ['id' => 'mark_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Models::className(), ['id' => 'model_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(DriverStatus::className(), ['id' => 'status_id']);
    }

    public function getStatuses()
    {
        $auto = DriverStatus::find()->all();
        return ArrayHelper::map($auto, 'id', 'name');
    }

    public function getApiStatus()
    {
        return ArrayHelper::map([
            ['id' => '1', 'type' => 'Сделано',],
            ['id' => '2', 'type' => 'Ошибка',],
            ['id' => '3', 'type' => 'Не сделанно',],
        ], 'id', 'type');
    }

    /**
     * @return int|string
     */
    public static function getActivatedDeiversCount()
    {
        return Drivers::find()->where(['status_id' => Drivers::DRIVER_STATUS_ACTIVATED])->andWhere(['creator_id' => Yii::$app->user->id])->count();
    }

    /**
     * Возвращает фимилию и инициалы
     *
     * @param $id
     * @return string
     */
    public static function getShortName($id)
    {
        $model = Drivers::find()->where(['id' => $id])->one();
        $n = mb_strtoupper(mb_substr($model->name, 0, 1)); //Получаем первую букву имени
        $mn = mb_strtoupper(mb_substr($model->middle_name, 0, 1)); //Первую букву отчества
        return $model->surname . ' ' . $n . '.' . $mn . '.';
    }

    /**
     * Метод генерации позывного (Буква из company.letter + первые буквы фамилии и имени + последние пять цифр телефона)
     *
     * @param $id
     * @return string
     */
    public static function getCallSign($id)
    {
        $model = Drivers::findOne($id);
        $name = mb_strtoupper(mb_substr($model->name, 0, 1)); //Получаем первую букву имени
        $surname = mb_strtoupper(mb_substr($model->surname, 0, 1)); //Первую букву фамилии
        $tel = $model->phone;
        $part_tel = str_replace(' ', '', mb_substr($tel, strlen($tel) - 6));

        Yii::info($surname . $name . $part_tel, 'test');

        $company_model = Company::findOne($model->company_id);

        isset($company_model) ? $letter = $company_model->letter : $letter = '';

        $call_sign = mb_strtoupper($letter) . $surname . $name . $part_tel;

        Yii::info('Позывной: ' . $call_sign, 'test');

        if (Drivers::find()->where(['callsign' => $call_sign])->one()->callsign) {
            //Если позывной уже занят - добавляем цифру
            $call_sign = $call_sign . rand(0, 9);
        }

        return $call_sign;

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverStatus()
    {
        return $this->hasMany(DriverStatus::className(), ['category_id' => 'id']);
    }

    /**
     * Удаление каталога с файлами
     *
     * @param string $dir Удаляемый каталог
     */
    public static function removeDirectory($dir)
    {
        if ($objs = glob($dir . "/*")) {
            foreach ($objs as $obj) {
                is_dir($obj) ? self::removeDirectory($obj) : unlink($obj);
            }
        }
        rmdir($dir);
    }

    /**
     * @param mixed $model app\models\Drivers model
     * @return array
     */
    public static function prepareParams($model)
    {

        $car_model = Car::find()->where(['driver_id' => $model->id])->one();

        $params = [
            'car' => [
                'id' => $car_model->car_company_outer_id,
                'mark' => $car_model->mark, //Например 'Audi'. Возможные Марки можно получить запросом getCarOptions (GET)
                'model' => $car_model->model,//Возможные Модели можно получить запросом getCarOptions (GET)
                'color' => $car_model->color,//Возможные Цвета можно получить запросом getCarOptions (GET)
                'registration_number' => $car_model->registration_number,
                'status' => $car_model->status,
                'year' => $car_model->year_manufacture,
                'car_license' => $car_model->car_license,
                'car_license_number' => $car_model->car_license_number,
                'car_date_license' => $car_model->car_date_license,
                'car_date_license_end' => $car_model->car_date_license_end,
            ],
            'driver' => [
                'id' => $model->driver_company_outer_id,
                'name' => $model->name,
                'last_name' => $model->surname,
                'middle_name' => $model->middle_name,
                'phone' => $model->phone,
                'bankid' => $model->callsign,
                'status' => $model->driver_status,
                'percenttocharge' => $model->percenttocharge,
                'creditlimit' => $model->creditlimit,
                'is_test' => $model->is_test,
//                'invited_by' => $invaited_by,
            ],
        ];

        if (!$model->driver_company_outer_id) {
            unset($params['driver']['id']);
        }

        if (!$car_model->car_company_outer_id) {
            unset($params['car']['id']);
        }

        Yii::info('Подготовленные параметры', 'test');
        Yii::info($params, 'test');

        return $params;
    }

    public static function arrPushPos($key, $value, $pos, $arr)
    {
        $new_arr = array();
        $i = 1;

        foreach ($arr as $arr_key => $arr_value) {
            if ($i == $pos) {
                $new_arr[$key] = $value;
            }

            $new_arr[$arr_key] = $arr_value;

            ++$i;
        }

        return $new_arr;
    }

    /**
     * Добавляет Водителя в сервис Ситимобил
     * @param mixed $model app\models\Drivers
     * @return array
     */
    public static function addDriverToCityMobil($model)
    {

        $car_model = Car::find()->where(['driver_id' => $model->id])->one();

        Yii::info('Код водителя перед отправкой: ' . $car_model->driver_id, 'test');

        //Формируем массив параметров
        $params = self::prepareParams($model);

        if (!count($params)) {
            return [false, 'Не сформирован массив параметров для АПИ запроса.'];
        }

        //Добавляем водителя и авто (createOrUpdateDriver (POST)
        try {
            $result = ClientController::requestAPI('createOrUpdateDriver', $params);
        } catch (Exception $e) {
            Yii::error($e->getMessage() . $e->getTraceAsString(), __METHOD__);
            return [false, 'Ошибка создания водителя в Ситимобил. Ошибка запроса АПИ'];
        }


        if (isset($result) && $result['success'] == 1) {
            if ($result['data']['driver_company_outer_id']) {

                $model->driver_company_outer_id = $result['data']['driver_company_outer_id'];
                $car_model->car_company_outer_id = $result['data']['car_company_outer_id'];

                Yii::info($model->driver_company_outer_id, 'test');
                Yii::info($car_model->car_company_outer_id, 'test');

                return [true, 'Ok'];
            } else {
                Yii::error($result['data']['msg'], __METHOD__);
                return [false, $result['data']['msg']];
            }
        } else {
            Yii::error($result['data']['msg'], __METHOD__);
            return [false, $result['data']['msg']];
        }
    }

    public static function getPhoto($id)
    {
        $model = self::findOne($id) ?? null;
        $img = "@web/images/nouser.png";

        if (isset($model)) {
            $directory = Yii::getAlias('@webroot/uploads/' . $id . '/profile');
            $files = array_diff(scandir($directory), array('..', '.'));

           if ($files[2]) $img = '@web/uploads/' . $id . '/profile/' . $files[2];
        }

        return Html::img($img, ['alt' => 'Фото водителя', 'class' => 'direct-chat-img']);
    }
}
