<?php

namespace app\models;

use app\controllers\DriversController;
use app\modules\api\controllers\ClientController;
use Yii;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "car".
 *
 * @property int $id
 * @property string $color Цвет
 * @property string $registration_number Регистрационный номер
 * @property string $status Статус машины. A - допущен, NM - не соответствует
 * @property string $car_license Номер разрешения
 * @property string $car_license_number Номер бланка
 * @property string $car_date_license Дата выдачи лицензии
 * @property string $car_date_license_end Дата окончания лицензии
 * @property string $mark Марка автомобиля
 * @property string $model Модель автомобиля
 * @property int $year_manufacture Год выпуска авто
 * @property string $sts_number Номер СТС
 * @property string $foto_sts_front_side Фото СТС лицевая сторона
 * @property string $foto_sts_reverse_side Фото СТС оборотная сторона
 * @property string $foto_license_front_side Фото Лицензии лицевая сторона
 * @property string $foto_license_reverse_side Фото Лицензии лицевая сторона
 * @property string $foto_dot_a Фото автомобиля с точка А
 * @property string $foto_dot_b Фото автомобиля с точка Б
 * @property string $foto_dot_v Фото автомобиля с точка В
 * @property string $foto_dot_g Фото автомобиля с точка Г
 * @property string $foto_salon Фото салона
 * @property int $driver_id Код водителя
 * @property string $car_company_outer_id Код автомобиля на CityMobile
 *
 * @property Drivers $driver
 */
class Car extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mark', 'model', 'year_manufacture', 'color', 'registration_number'], 'required'],
            [['car_date_license', 'car_date_license_end'], 'safe'],
            [['car_company_outer_id'], 'string'],
            [['year_manufacture', 'driver_id'], 'integer'],
            [['model', 'mark', 'color', 'registration_number', 'status', 'car_license', 'car_license_number', 'sts_number', 'foto_sts_front_side', 'foto_sts_reverse_side', 'foto_license_front_side', 'foto_license_reverse_side', 'foto_dot_a', 'foto_dot_b', 'foto_dot_v', 'foto_dot_g', 'foto_salon'], 'string', 'max' => 255],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Drivers::className(), 'targetAttribute' => ['driver_id' => 'id']],
        ];
    }
    public function beforeSave($insert){
        $car = Yii::$app->request->post('Car');
        $car_date_license = str_replace('.', '-',$car['car_date_license']);
        $car_date_license_end = str_replace('.', '-',$car['car_date_license_end']);
        $this->car_date_license = date('Y-m-d', strtotime($car_date_license));
        $this->car_date_license_end = date('Y-m-d', strtotime($car_date_license_end));
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color' => 'Цвет',
            'registration_number' => 'Гос. номер',
            'status' => 'Статус',
            'car_license' => 'Номер разрешения',
            'car_license_number' => 'Номер бланка разрешения',
            'car_date_license' => 'Дата выдачи разрешения',
            'car_date_license_end' => 'Дата окончания разрешения',
            'mark' => 'Марка',
            'model' => 'Модель',
            'year_manufacture' => 'Год выпуска',
            'sts_number' => 'Номер СТС',
            'foto_sts_front_side' => 'Фото СТС лицевая сторона',
            'foto_sts_reverse_side' => 'Фото СТС обратная сторона',
            'foto_license_front_side' => 'Фото разрешения, лицевая сторона',
            'foto_license_reverse_side' => 'Фото разрешения обратная сторона',
            'foto_dot_a' => 'Foto Dot A',
            'foto_dot_b' => 'Foto Dot B',
            'foto_dot_v' => 'Foto Dot V',
            'foto_dot_g' => 'Foto Dot G',
            'foto_salon' => 'Foto Salon',
            'driver_id' => 'Код водителя',
            'car_company_outer_id' => 'Код автомобиля в CityMobil',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Drivers::className(), ['id' => 'driver_id']);
    }


    /**
     * @return bool|string
     */
    public static function getCarOptions()
    {
//        $result = self::requestAPI('getCarOptions', [], 'GET');
        $result = ClientController::requestAPI('getCarOptions', [], 'GET');
        return $result['data'];
    }

//    public static function requestAPI($method, $params, $send_method = 'POST')
//    {
//        $version = '1.0.0';
//        $auth_key = Settings::find()->where(['key' => 'api_key'])->one()->value;
//        $url = "https://partner-api.city-mobil.ru/{$version}/{$method}";
//
//        if ($send_method == 'GET') {
//            $url .= "?auth_key={$auth_key}";
//            //Собираем ссылку из параметров
//            foreach ($params as $key => $value) {
//                $url .= '&' . $key . '=' . $value;
//            }
//        } else {
//            $params['auth_key'] = $auth_key;
//        }
//
//
//        Yii::warning($url, __METHOD__);
//
//
//        $result = file_get_contents($url, false, stream_context_create([
//            'http' => [
//                'request_fulluri' => true,
//                'method' => $send_method,
//                'header' => 'Content-type: application/x-www-form-urlencoded',
//                'content' => http_build_query($params)
//            ]
//        ]));
//
//        $result = Json::decode($result);
//
//        if ($send_method == 'POST')
//            Yii::warning($result);
//
//        return $result;
//
//    }
}
