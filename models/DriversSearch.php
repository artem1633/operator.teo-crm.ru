<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DriversSearch represents the model behind the search form about `app\models\Drivers`.
 */
class DriversSearch extends Drivers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'need_sticker', 'is_test', 'creditlimit', 'id', 'status_id', 'creator_id', 'activator_id', 'status_api', 'mark_id', 'model_id', 'year_manufacture'], 'integer'],
            [['percenttocharge'], 'number'],
            [['passport_number', 'data_sto', 'invited_by', 'driver_status', 'surname', 'name', 'middle_name', 'phone', 'birthday', 'password_number', 'issued_by', 'issue_date_passport', 'number_bu', 'class_bu', 'issue_date_bu', 'callsign', 'foto_passport', 'foto_propiska', 'foto_bu_front_side', 'foto_bu_reverse_side', 'foto_driver', 'date_create', 'date_activation', 'comment', 'state_number', 'sts_number', 'resolution', 'license', 'foto_sts_front_side', 'foto_sts_reverse_side', 'foto_license_front_side', 'foto_license_reverse_side', 'foto_dot_a', 'foto_dot_b', 'foto_dot_v', 'foto_dot_g', 'foto_salon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (Users::isActivator()){
            //Выбираем водителей у которых активатор = текущий пользователь или статус водителя != "На активации"
            $query = Drivers::find()->where(['activator_id' => Yii::$app->user->id])->orWhere(['NOT', ['status_id' => 2]]);
        } else {
            $query = Drivers::find();
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query->orderBy(['date_create' => SORT_DESC, 'id' => SORT_DESC]),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'issue_date_passport' => $this->issue_date_passport,
            'issue_date_bu' => $this->issue_date_bu,
            'status_id' => $this->status_id,
            'creator_id' => $this->creator_id,
            'activator_id' => $this->activator_id,
            'date_create' => $this->date_create,
            'date_activation' => $this->date_activation,
            'status_api' => $this->status_api,
            'mark_id' => $this->mark_id,
            'model_id' => $this->model_id,
            'year_manufacture' => $this->year_manufacture,
            'percenttocharge' => $this->percenttocharge,
            'creditlimit' => $this->creditlimit,
            'is_test' => $this->is_test,
            'need_sticker' => $this->need_sticker,
            'company_id' => $this->company_id,
        ]);

        $query->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'password_number', $this->password_number])
            ->andFilterWhere(['like', 'issued_by', $this->issued_by])
            ->andFilterWhere(['like', 'number_bu', $this->number_bu])
            ->andFilterWhere(['like', 'class_bu', $this->class_bu])
            ->andFilterWhere(['like', 'callsign', $this->callsign])
            ->andFilterWhere(['like', 'foto_passport', $this->foto_passport])
            ->andFilterWhere(['like', 'foto_propiska', $this->foto_propiska])
            ->andFilterWhere(['like', 'foto_bu_front_side', $this->foto_bu_front_side])
            ->andFilterWhere(['like', 'foto_bu_reverse_side', $this->foto_bu_reverse_side])
            ->andFilterWhere(['like', 'foto_driver', $this->foto_driver])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'state_number', $this->state_number])
            ->andFilterWhere(['like', 'sts_number', $this->sts_number])
            ->andFilterWhere(['like', 'resolution', $this->resolution])
            ->andFilterWhere(['like', 'license', $this->license])
            ->andFilterWhere(['like', 'foto_sts_front_side', $this->foto_sts_front_side])
            ->andFilterWhere(['like', 'foto_sts_reverse_side', $this->foto_sts_reverse_side])
            ->andFilterWhere(['like', 'foto_license_front_side', $this->foto_license_front_side])
            ->andFilterWhere(['like', 'foto_license_reverse_side', $this->foto_license_reverse_side])
            ->andFilterWhere(['like', 'foto_dot_a', $this->foto_dot_a])
            ->andFilterWhere(['like', 'foto_dot_b', $this->foto_dot_b])
            ->andFilterWhere(['like', 'foto_dot_v', $this->foto_dot_v])
            ->andFilterWhere(['like', 'foto_dot_g', $this->foto_dot_g])
            ->andFilterWhere(['like', 'foto_salon', $this->foto_salon])
            ->andFilterWhere(['like', 'invited_by', $this->invited_by])
            ->andFilterWhere(['like', 'passport_number', $this->passport_number])
            ->andFilterWhere(['like', 'driver_status', $this->driver_status]);

        return $dataProvider;
    }
}
