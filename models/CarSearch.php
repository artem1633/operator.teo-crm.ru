<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Car;

/**
 * CarSearch represents the model behind the search form about `app\models\Car`.
 */
class CarSearch extends Car
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year_manufacture', 'driver_id'], 'integer'],
            [['mark', 'model', 'color', 'registration_number', 'status', 'car_license', 'car_license_number', 'car_date_license', 'car_date_license_end', 'sts_number', 'foto_sts_front_side', 'foto_sts_reverse_side', 'foto_license_front_side', 'foto_license_reverse_side', 'foto_dot_a', 'foto_dot_b', 'foto_dot_v', 'foto_dot_g', 'foto_salon', 'id_api'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Car::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'car_date_license' => $this->car_date_license,
            'car_date_license_end' => $this->car_date_license_end,
            'year_manufacture' => $this->year_manufacture,
            'driver_id' => $this->driver_id,
        ]);

        $query->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'registration_number', $this->registration_number])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'car_license', $this->car_license])
            ->andFilterWhere(['like', 'car_license_number', $this->car_license_number])
            ->andFilterWhere(['like', 'sts_number', $this->sts_number])
            ->andFilterWhere(['like', 'foto_sts_front_side', $this->foto_sts_front_side])
            ->andFilterWhere(['like', 'foto_sts_reverse_side', $this->foto_sts_reverse_side])
            ->andFilterWhere(['like', 'foto_license_front_side', $this->foto_license_front_side])
            ->andFilterWhere(['like', 'foto_license_reverse_side', $this->foto_license_reverse_side])
            ->andFilterWhere(['like', 'foto_dot_a', $this->foto_dot_a])
            ->andFilterWhere(['like', 'foto_dot_b', $this->foto_dot_b])
            ->andFilterWhere(['like', 'foto_dot_v', $this->foto_dot_v])
            ->andFilterWhere(['like', 'foto_dot_g', $this->foto_dot_g])
            ->andFilterWhere(['like', 'foto_salon', $this->foto_salon])
            ->andFilterWhere(['like', 'mark', $this->mark])
            ->andFilterWhere(['like', 'model', $this->model]);

        return $dataProvider;
    }
}
